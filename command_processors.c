
#include "command_processors.h"
#include "pressure.h"
#include "eeprom.h"

#ifdef BUILD_GPS
static int err = 0;
#endif // BUILD_GPS

int soft_reset(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) { // local command, so do it
        printf("Resetting...\n\r");
        tb_delay_ms(1000);
        printf("\033[2J"); // clear screen
        tb_delay_ms(100);
        __asm__ volatile ("reset");
    } else { // command for other module, so pass it on
        send_command(fields, 1, fields[1]); // send RESET to slave
        printf("Resetting module %s\n\r", fields[1]);
    }
    return 0;
}

int sleep(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
        printf("Shutting down...");
        __asm__ volatile ("pwrsav #0");
    } else if (IS_MASTER) send_command(fields, 1, fields[1]);
    printf("Shutting down module %s\n\r", fields[1]);
    return 0;
}

int audio_on(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) pwm_timer_set(6, 1000, 4000, ALARM, NOINVERT);
    else if (IS_MASTER) send_command(fields, 1, fields[1]);
    return 0;
}

int vbat(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_VOLTMETER
        long V;
        char voltage[6]; // buffer to hold results string
        char* content;
        V = ((long) (adc_get()) * 1000) / 656;
        sprintf(voltage, "%ld.%.2ld", V / 100, V % 100);
        fields[0] = "BATT";
        fields[1] = voltage;
        content = build_frm(get_msg_buffer(MISO), fields, 2, START_FLAG, END_FLAG, DELIMITER);
        printf("%s\n\r", content);
#else
        printf("Voltmeter not installed.\n\r");
#endif // BUILD_VOLTMETER
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(5);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int audio_off(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) pwm_timer_set(6, 0, 0, ALARM, NOINVERT);
    else if (IS_MASTER) send_command(fields, 1, fields[1]);
    return 0;
}

int pwm(char* fields[]) {
    int i;
    int p[5];
    if ((*fields[6] == '0') || (*fields[6] == 0)) {
        for (i = 1; i < 6; i++) p[i - 1] = atoi(fields[i]);
        pwm_timer_set(p[0], p[1], p[2], p[3], p[4]);
    } else if (IS_MASTER) send_command(fields, 6, fields[6]);
    return 0;
}

int acknowledge(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
        *(get_msg_buffer(MISO)) = 0;
        ClrWdt();
//        printf("ACK\n\r");
    } else {
        send_command(fields, 1, fields[1]);
    }
    return 0;
}

int write_nvm(char* fields[]) {
    int err = 0;
    int page;
    if ((*fields[3] == '0') || (*fields[3] == 0)) {
        page = (atoi(fields[1]));
        if (page < NVM_PAGES) { // ensure page is in range
            if (!((str_len(fields[2]) + 1) > NVM_PAGE_SIZE)) { // if string not too long
                nvm_write_string((page * NVM_PAGE_SIZE), fields[2]);
            } else err = 1; // string too long
        } else err = 1; // page out of range
    } else send_command(fields, 3, fields[3]);
    return err;
}

int read_nvm(char* fields[]) {
    int err = 0;
    int page;
    if ((*fields[2] == '0') || (*fields[2] == 0)) {
        page = (atoi(fields[1]));
        if (page < NVM_PAGES) { // ensure page is in range
            nvm_read_string((atoi(fields[1]) * NVM_PAGE_SIZE), get_msg_buffer(MISO));
            printf("%s\n\r", get_msg_buffer(MISO));
        } else err = 1; // page out of range  
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 2, fields[1]);
        tb_delay_ms(NVM_DELAY);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[2]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return err;
}

int light(char* fields[]) {
    int err = 0;
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_LIGHT
        int i;
        char results[SI1133_CHANNEL_CNT][SI1133_RESULT_FIELD_SIZE];
        char* content;
        for (i = 0; i < SI1133_CHANNEL_CNT; i++) si1133_channel_select(i, 1);
        si1133_setup_channels(); // setup channels in sensor
        si1133_force();
        tb_delay_ms(SI1133_DELAY);
        add_token_ptr(0, "LITE");
        for (i = 0; i < SI1133_CHANNEL_CNT; i++) {
            sprintf(results[i], "%ld", si1133_results(i));
            add_token_ptr(i + 1, results[i]);
        }
        content = build_frm(get_msg_buffer(MISO), fields, 7, START_FLAG, END_FLAG, DELIMITER);
        if(get_debug()) printf("%s\n\r", content);
#else 
        printf("Light sensor not installed.\n\r");
#endif // BUILD_LIGHT
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        int n = atoi(fields[1]);
        char id[2];
        copy_string(id, fields[1]);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(SI1133_DELAY);
        readSI2C1((I2C_SLAVE_BASE_ADR + n - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        if (buffer != parse_frm(buffer, fields, START_FLAG, END_FLAG, DELIMITER, 1)) {
            if (str_match("LITE", fields[0], 4, 0)) {
                fields[0] = "ACK";
                send_command(fields, 1, id);
            }
        }
        *buffer = 0;
        }
#endif // BUILD_CENTRAL_PROCESSOR
    return err;
}

int prep_log(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_EEPROM
        printf("Erasing log...");
        log_erase(LOG_START);
        pwm_timer_set(ERROR, 0, 0, LED2, NOINVERT);
        printf("Erased.\n\r");
#else
    printf("Log not installed\n\r");
#endif // BUILD_EEPROM
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 1, fields[1]);
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int dump_log(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) { // dump log;      
#ifdef BUILD_EEPROM
        int page;
        page = LOG_START;
        while ((page < log_next_page(0)) && !get_msg_flag(UI)) {// if CR entered, stop report
            printf("%d, %s\n\r", page, log_read(get_msg_buffer(LOG), page, 0));
            page++;
//            wdt_clr();
        }
#else
        printf("Log not installed\n\r");
#endif // BUILD_EEPROM
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        int pagenum;
        int end_found = 0;
        char s_page[5];
        char id[2];
        char destination[2]; // destination module
        copy_string(destination, fields[1]);
        char* buffer = get_msg_buffer(MISO);
        pagenum = LOG_START;
        while ((!end_found) && (pagenum < EEPROM_PAGES) && !get_msg_flag(UI)) { // if CR entered, stop report
            sprintf(s_page, "%d", pagenum); // convert the page number to a string
            fields[0] = "PAGE"; // prep the command
            fields[1] = s_page; // include the page number
            send_command(fields, 2, destination);
            tb_delay_ms(LOG_DELAY);
            sprintf(id, "%d", MODULE_ID_LOG);
            fields[0] = "ACK"; // build an acknowledge message
            send_command(fields, 1, id); // send it
            tb_delay_ms(LOG_DELAY);
            readSI2C1((I2C_SLAVE_BASE_ADR + atoi(destination) - 1) << 1, buffer);
            if (str_len(buffer) != 0) {
                printf("%d: %s\n\r", pagenum, buffer);
                pagenum++;
            }
            else {
                end_found = 1;
                *buffer = 0;
            }
        }
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int get_page(char* fields[]) {
    if ((*fields[2] == '0') || (*fields[2] == 0)) { // get page from log;      
#ifdef BUILD_EEPROM
        char* buffer = get_msg_buffer(MISO);
        int page_num = atoi(fields[1]);
        if (page_num < log_next_page(0)) {
            printf("%d, %s\n\r", page_num, log_read(buffer, page_num, 0));
        } else *buffer = 0;
#else
        printf("Log not installed\n\r");
#endif // BUILD_EEPROM
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 2, fields[2]);
        tb_delay_ms(LOG_DELAY);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[2]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int log_entry(char* fields[]) {
    int n;
    if ((*fields[1] == '0') || (*fields[1] == 0)) return 1; // must have data
    else {
        n = atoi(fields[1]); // number of fields
        if ((*fields[n + 2] == '0') || (*fields[n + 2] == 0)) {
#ifdef BUILD_EEPROM
            int i;
            char* ptr;
            char* content;
            int j;
            int err;
            char* buffer = get_msg_buffer(MISO);
            for (i = 2; i < (n + 2); i++) fields[i - 2] = fields[i];
            ptr = buffer;
            for (j = 0; j < LOG_BFR_SIZE; j++) { // clear buffer
                *ptr = 0;
                ptr++;
            }
            content = build_frm(buffer, fields, n, START_FLAG, END_FLAG, DELIMITER);
            if (log_next_page(0) == (EEPROM_PAGES - 1)) {
                pwm_timer_set(ERROR, ERR_PD, ERR_PD, LED2, NOINVERT);
                printf("Log full!\n\r");
            }
            else {
                err = log_write(content, log_next_page(1));
                if (err != 0) {
                    printf("Data Log Error: %d\n\r", err_clear(DATA_LOG));
                    *buffer = 0;
                } else {
                    printf("Logged: %s\n\r", buffer);
                    log_erase(log_next_page(0)); // put end marker at next page
                }
            }
#else
            printf("Log not installed\n\r");
#endif // BUILD_EEPROM
        }
#ifdef BUILD_CENTRAL_PROCESSOR
        else send_command(fields, n + 2, fields[n + 2]);
#endif // BUILD_CENTRAL_PROCESSOR
    }
    return 0;
}

int status_log(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_EEPROM
        char* content;
        char size[5]; // array to hold log size string
        char used[5]; // array to hold log pages used string
        add_token_ptr(0, "PAGES");
        sprintf(size, "%d", log_size());
        sprintf(used, "%d", log_used());
        add_token_ptr(1, size);
        add_token_ptr(2, used);
        content = build_frm(get_msg_buffer(MISO), fields, 3, START_FLAG, END_FLAG, DELIMITER);
        printf("%s\n\r", content);
#else
        printf("Log not installed\n\r");
#endif // BUILD_EEPROM
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(2);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int gps_test(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {

#ifdef BUILD_GPS
        //    if ((*fields[1] == '0') || (*fields[1] == 0)) clr_ip_flag(IPF_0);
        //    else if (*fields[1] == '1') set_ip_flag(IPF_0);
        clr_msg_flag(GPS);
        gps_enable();
        *(get_msg_buffer(MISO)) = 0; // clear MISO buffer
        while (!get_msg_flag(GPS));
        copy_string(get_msg_buffer(MISO), get_msg_buffer(GPS));
        printf("%s\n\r", get_msg_buffer(GPS));
        err = gps_process_msg();
        if (err == 0) {
            gps_process_msg();
            printf("Time: %s UTC\n\r", gps_field(TIME));
            printf("Latitude: %s %s\n\r", gps_field(LATITUDE), gps_field(NS));
            printf("Longitude: %s %s\n\r", gps_field(LONGITUDE), gps_field(EW));
            printf("Altitude: %s m\n\r", gps_field(ALTITUDE));
            printf("NavStat: %s\n\r", gps_field(NAVSTAT));
            printf("Speed: %s km/h\n\r", gps_field(SPEED));
            printf("Course: %s deg\n\r", gps_field(DEG));
            printf("Vert: %s m/s\n\r", gps_field(VVEL));
            printf("Satellites: %s\n\r", gps_field(SATS));
        } else printf("error code: %d\n\r", err);
#endif // BUILD_GPS
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(1003); // allow time for gps module to get result
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int led_test(char* fields[]) {
    if ((*fields[2] == '0') || (*fields[2] == 0)) {
#if ((PCB_MODEL == 1) || (PCB_MODEL == 2))
        if ((*fields[1] == '0') || (*fields[1] == 0)) {
            pwm_timer_set(5, 0, 0, LED2, NOINVERT);
            pwm_timer_set(4, 0, 0, LED1, NOINVERT);
            pwm_timer_set(6, 0, 0, DO1, NOINVERT);
        } else if (*fields[1] == '1') {
            pwm_timer_set(5, 250, 250, LED2, INVERT);
            pwm_timer_set(4, 250, 250, LED1, NOINVERT);
            pwm_timer_set(6, 250, 250, DO1, NOINVERT);
        }
#else
        printf("LED test not available on this board.\n\r");
#endif
    } else if (IS_MASTER) send_command(fields, 2, fields[2]);
    return 0;
}

int switch_test(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#if ((PCB_MODEL == 1) || (PCB_MODEL == 2))
        printf("Press switch while left LED lit.  Confirm right LED lights.\n\r");
        pwm_timer_set(6, 10000, 0, 0, 0);
        pwm_timer_set(4, 10000, 0, LED1, NOINVERT);
        while (!pwm_timer(6)); // wait until timer started
        while (pwm_timer(6)) DOUT5 = ~PORTBbits.RB4; // led when switch pressed
        DOUT5 = 0; // test timer finished, turn off led
        printf("Switch test completed.\n\r");
#else
        printf("Switch test not available on this board.\n\r");
#endif
    } else if (IS_MASTER) {
        send_command(fields, 1, fields[1]);
        printf("Press switch while left LED lit.  Confirm right LED lights.\n\r");
    }
    return 0;
}

int initiate_measurement(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_GPS
        clr_msg_flag(GPS); // clear message flag as msg will be overwritten
        gps_enable();
#else
        printf("GPS not installed.\n\r");
#endif // BUILD_GPS
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 1, fields[1]); // forward command
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int get_results(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_GPS
        copy_string(get_msg_buffer(MISO), get_msg_buffer(GPS));
        if (get_debug()) printf("%s\n\r", get_msg_buffer(GPS));
#else
        printf("GPS not installed.\n\r");
#endif // BUILD_GPS
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(3);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int pressure(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
#ifdef BUILD_PRESSURE
        long P; // compensated pressure value
        long T; // compensated temperature value
        char temperature[8]; // result string
        char pressure[8]; // result string
        char* content;
        pressure_values(&T, &P);
        sprintf(temperature, "%ld", T);
        sprintf(pressure, "%ld", P);
        fields[0] = "PRES";
        fields[1] = temperature;
        fields[2] = pressure;
        content = build_frm(get_msg_buffer(MISO), fields, 3, START_FLAG, END_FLAG, DELIMITER);
        printf("%s\n\r", content);

#else
        printf("Pressure module not installed.\n\r");
#endif // BUILD_PRESSURE 
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(PRESS_DELAY);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int temperature(char* fields[]) {
        long V;
        char voltage[6];
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
        V = 0;
        voltage[0] = '\0'; // buffer to hold results string
#ifdef BUILD_VOLTMETER
        V = ((long) (adc_get()) * 1000) / 656;
        sprintf(voltage, "%ld", V);
#endif // BUILD_VOLTMETER
#ifdef BUILD_TEMPERATURE
        char temperature[9];
        char* content;
        int T = mcp9802_readtemp(SENSOR1);
        sprintf(temperature, "%d", T);
        fields[0] = "TEMP";
        fields[1] = temperature;
        fields[2] = voltage;
        content = build_frm(get_msg_buffer(MISO), fields, 3, START_FLAG, END_FLAG, DELIMITER);
        if (i2c_err()) {
            printf("I2C Error %d\n\r", i2c_err());
            clr_i2c_err();
        } 
        else printf("%s\n\r", content);
#else
        printf("Temperature module not installed\n\r");
#endif // BUILD_TEMPERATURE    
        }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        char* buffer = get_msg_buffer(MISO);
        send_command(fields, 1, fields[1]);
        tb_delay_ms(TEMP_DELAY);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[1]) - 1) << 1, buffer);
        printf("%s\n\r", buffer);
        *buffer = 0; // clear MISO buffer by inserting End of String character
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int save_config(char* fields[]) {
    char* buffer = get_msg_buffer(MISO);
    int n;
    int destfield;
    if (*fields[1] == '0') destfield = 2; // only 2 arguments if display mode '0'
    else destfield = 3; // 3 arguments if setting config value
    if ((*fields[destfield] == '0') || (*fields[destfield] == '\0')) { // if local module
        nvm_read_string(NVM_CONFIG_PAGE, buffer); // get the config frame
        n = atoi(fields[1]);
        if (n) {
            replace_token(buffer, fields, n, fields[2], START_FLAG, END_FLAG, DELIMITER);
            nvm_write_string(NVM_CONFIG_PAGE, buffer); // write the string to nvm
            nvm_read_string(NVM_CONFIG_PAGE, buffer); // read it back
            printf("Saved: ");
        }
        printf("%s\n\r", buffer); // print it
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        send_command(fields, destfield, fields[destfield]);
        tb_delay_ms(10);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[destfield]) - 1) << 1, buffer);
        printf("Saved: %s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int config_cycle(char* fields[]) {
    char* buffer = get_msg_buffer(MISO);
    int c = 0; // indicates cycle timer
    int destfield = 3;
    if ((*fields[1] == 'L') || (*fields[1] == 'l')) c = 1; // long cycle
    else if ((*fields[1] == 'M') || (*fields[1] == 'm'))  c = 2; // medium cycle
    else if ((*fields[1] == 'S') || (*fields[1] == 's')) c = 3; // short cycle
    if ((*fields[destfield] == '0') || (*fields[destfield] == '\0')) { // if local module
        nvm_read_string(NVM_CONFIG_PAGE, buffer); // get the config frame
        if (c) {
            replace_token(buffer, fields, c, fields[2], START_FLAG, END_FLAG, DELIMITER);
            nvm_write_string(NVM_CONFIG_PAGE, buffer); // write the string to nvm
            nvm_read_string(NVM_CONFIG_PAGE, buffer); // read it back
            printf("Saved: ");
        }
        printf("%s\n\r", buffer); // print it
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else {
        send_command(fields, destfield, fields[destfield]);
        tb_delay_ms(10);
        readSI2C1((I2C_SLAVE_BASE_ADR + atoi(fields[destfield]) - 1) << 1, buffer);
        printf("Saved: %s\n\r", buffer);
        *buffer = 0;
    }
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int run_cycle(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
        cycle_clock_run();
        wdt(ON); // turn on watchdog timer
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 1, fields[1]); // send to slave
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int pause_cycle(char* fields[]) {
    if ((*fields[1] == '0') || (*fields[1] == 0)) {
        cycle_clock_pause();
        wdt(OFF); // turn off watchdog timer
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 1, fields[1]); // send RESET to slave
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int config_ticks(char* fields[]) {
#ifdef BUILD_EEPROM
//    return config_item(TICKS, fields);
    return 0;
#else
    return 1;
#endif // BUILD_EEPROM
}

int config_osctun(char* fields[]) {
#ifdef BUILD_EEPROM
//    return config_item(OSC_TUN, fields);
    return 0;
#else
    return 1;
#endif // BUILD_EEPROM
}

int trace_msg(char* fields[]) {
    if ((*fields[2] == '0') || (*fields[2] == 0)) {
        set_trace(atoi(fields[1]));
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 2, fields[2]); // send to slave
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int debugger(char* fields[]) {
    if ((*fields[2] == '0') || (*fields[2] == 0)) {
        set_debug(atoi(fields[1]));
    }
    
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 2, fields[2]); // send to slave
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

int ip_flag_msg(char* fields[]) {
    if ((*fields[3] == '0') || (*fields[3] == 0)) {
        if(atoi(fields[2])) set_ip_flag(atoi(fields[1]));
        else clr_ip_flag (atoi(fields[1]));
    }
#ifdef BUILD_CENTRAL_PROCESSOR
    else send_command(fields, 3, fields[3]); // send to slave
#endif // BUILD_CENTRAL_PROCESSOR
    return 0;
}

void show_err(int err_num) {
    switch (err_num) {
        case 1:
            printf("Invalid input.\n\r");
            break;
        case 2:
            printf("Unrecognized command.\n\r");
            break;
        default:
            break;
    }
}

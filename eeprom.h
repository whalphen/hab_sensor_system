/* 
 * File:   eeprom.h
 * Author: WHalphen
 *
 * Created on April 12, 2016, 9:05 AM
 */

#ifndef EEPROM_H
#define	EEPROM_H

/* Functions to write and read eeprom and functions for logging data
 * FUNCTION
 *  char ctl_calc(char dev_addr, unsigned long eeprom_addr);
 * PURPOSE
 *  local function to create the control value for the eeprom and send address
 *  sequence
 * PARAMS
 *  receives the address of the eeprom device and the address to be accessed
 *  in the eeprom
 * RETURNS control value
 * 
 * 
 * int eeprom_writebyte(unsigned long eeprom_addr, char eeprom_data);
 * int eeprom_writepage(unsigned long eeprom_addr, char eeprom_data);
 * char eeprom_readbyte(unsigned long eeprom_addr);
 * int eeprom_seq_read(unsigned long eeprom_addr, char* bufr, int cnt);
 * 
 * FUNCTION
 *  int log_init(char* log_bufr);
 *  initializes log by setting variables used in managing the log
 *  this function should be executed once during startup
 * 
 * FUNCTION
 *  int log_write(char* bufr, int page);
 *  writes the message in bufr to the log position specified by page
 *  returns non-zero value if write failed, returns 0 if no errors
 *  total length of the message cannot exceed 128 bytes, including string
 *  string terminator
 * 
 * FUNCTION
 *  char* log_read(char* bufr, int page, int count);
 *  reads the log entry specified by page to bufr
 *  reads the number of characters specified by count
 *  if count is zero, reads until a string terminator is found
 *  returns non-zero value if read failed, returns 0 if no errors
 *  the bufr length must be at least 128 bytes
 * 
 * FUNCTION
 *  int get_log_start(void);
 *  Returns the index of page one of the log. One or more pages may have been
 *  reserved for system use, so start page may be greater than 0.
 * 
 * FUNCTION
 *  int log_next_page(int incr);
 *  Returns the index of the next entry position in the log
 *  If the log is empty, returns 0. If the log is full, returns log length + 1
 *  If incr is not zero, the index is incremented after current position is
 *  obtained.
 * 
 * FUNCTION
 *  int get_log_len(void);
 *  Returns the number of entry positions in the log, including used and 
 *  unused pages
 * 
 * FUNCTION
 *  int log_erase(int begin);
 *  Erases all entries (except the first one) from the log and prepares it for 
 *  use.  The first page of the log is not affected by the new_log function and
 *  is used for system configuration.  Returns a non-zero value if an error
 *  occurred.
 * 
 * FUNCTION
 * void log_report(int from, int to);
 *  Sends content of log to user interface for range specified by from and to
 *  page numbers.
 * 
 * FUNCTION
 * char* log_status(char* bufr);
 *  Returns a pointer to the log status (length and used) string.
 * 
 * FUNCTION
 * int config_item(int index, char* value, char* result, int rw);
 *  Stores or fetches a configuration item. Use 'index' to specify which
 *  config item to reference. Provide pointer to value to write 
 *  (parameter: value) and provide pointer for result (parameter: result).
 *  Use 'rw' parameter indicate whether action is to be read (0) or 
 *  write (1).
 *
 */

#include "sys_config.h"
#include "utilities.h"
#ifdef BUILD_EEPROM
#include "i2c_driver.h"

#define A18_MASK 0x00040000
#define A17_MASK 0x00020000
#define A16_MASK 0x00010000

char addr_seq(unsigned long eeprom_addr);
int eeprom_writebyte(unsigned long eeprom_addr, char eeprom_data);
int eeprom_writepage(unsigned long eeprom_addr, char* eeprom_data);
char eeprom_readbyte(unsigned long eeprom_addr);
int eeprom_seq_read(unsigned long eeprom_addr, char* bufr, int cnt);

int log_init(char* log_bufr);
int get_log_start(void);
int get_log_len(void);
int log_write(char* bufr, int page);
char* log_read(char* bufr, int page, int count);
int log_erase(int begin);
//void log_report(int from, int to);
int log_next_page(int incr);
char* log_status(char* bufr);
int log_size(void);
int log_used(void);

#endif //BUILD_EEPROM
void nvm_write(int addr, int data);
int nvm_read (int addr);
int nvm_read_string(int adr, char* buffer);
void nvm_write_string(int adr, char* s);

#endif	/* EEPROM_H */


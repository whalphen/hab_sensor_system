

#include "msg_processor.h"

#ifdef BUILD_GPS
char csmsb;
char cslsb;
#endif //BUILD_GPS

static char* token_ptrs[MAX_TOKENS];
const char null_string = '\0'; // used to set token_ptrs to point to null

char** get_token_ptrs(void) { // get ptr to array of ptrs to token strings
    return token_ptrs;
}

void clear_token_ptrs(void) { // point all token ptrs to null string
    int i;
    for (i = 0; i < MAX_TOKENS; i++) {
        token_ptrs[i] = (char*) &null_string;
    }
}

void add_token_ptr(int position, char* pointer) {
    token_ptrs[position] = pointer;
}
struct mailslot {
    int flg;
    char* mailbox;
};

volatile static struct mailslot mailslots[NUM_MAILSLOTS];

volatile static int ip_flag[NUM_IP_FLAGS]; // inter-process flags

typedef int (*command_ptr)(char**); // function pointer for commands

struct keyword_table{
    char keyword[KEYWORD_MAX_LEN]; // keyword for function to be executed
    int match_len; // minimum # characters required for match
    command_ptr command; // function pointer
};

const struct keyword_table keywords[] = {
    // keyword, match_len, function
    {"RESET", 3, soft_reset},
    {"SHUTDOWN", 2, sleep},
    {"DUMP LOG", 2, dump_log},
    {"NEW LOG", 2, prep_log},
    {"E", 1, initiate_measurement},
    {"MEASURE", 2, initiate_measurement},
    {"F", 1, get_results},
    {"RESULTS", 4, get_results},
    {"ENTRY", 2, log_entry},
    {"LOG", 2, status_log},
    {"AUDIO", 2, audio_on},
    {"ACK", 2, acknowledge},
    {"SILENT", 2, audio_off},
    {"PWM", 3, pwm},
    {"GPS TEST", 3, gps_test},
    {"PRESSURE", 1, pressure},
    {"CONFIG", 3, save_config},
    {"LED TEST", 3, led_test},
    {"SWITCH", 3, switch_test},
    {"TEMP", 1, temperature},
    {"RUN", 3, run_cycle},
    {"PAUSE", 3, pause_cycle},
    {"BATTERY", 3, vbat},
    {"CYCLE", 3, config_cycle},
    {"TICKS", 4, config_ticks},
    {"OSC TUN", 3, config_osctun},
    {"WRITE", 3, write_nvm}, // write nvm
    {"READ", 3, read_nvm}, // read nvm
    {"LIGHT", 2, light}, // read light sensor
    {"PAGE", 3, get_page}, // read page from log
    {"TRACE", 3, trace_msg},
    {"DEBUG", 2, debugger},
    {"FLAG", 3, ip_flag_msg},
    {"\0", 0, 0}
};

void preprocess(char* ptr){
    int has_sf = 0;
    char insert = START_FLAG;;
    char temp;
    if(*ptr == START_FLAG) has_sf = 1;
    while((insert != 0) && (insert != END_FLAG)) {
        if(!has_sf) { //no start flag, so insert one and push out rest of chars
            temp = *ptr;
            *ptr = insert;
            insert = temp;
        }
        else insert = *ptr; // has a start flag, so search for end flag
        ptr++;
    }
    if(insert == 0) { // no end flag found, so add one
        *ptr = END_FLAG; // overwrites string terminator
        ptr++;
        *ptr = 0; // add new string terminator
    }
}

int process_msg(char* fields[], exchanges mailslot) {
    int err = 0;
    char* inptr;
    int i;
    int found = 0;
    int fl;
    if (get_msg_flag(mailslot)) {
        clr_msg_flag(mailslot);
        for (i = 0; i < MAX_TOKENS; i++) { // point all field ptrs to null
            fields[i] = (char*) &null_string;
        }
        if (mailslot == UI) preprocess(get_msg_buffer(mailslot));
        else if (mailslot == MOSI) {
            pwm_timer_set(COMM_PULSE, PULSE_LEN, 0, LED2, NOINVERT);
            if(get_trace()) printf("Processing Msg: %s\n\r", get_msg_buffer(mailslot));
        }
        inptr = parse_frm(get_msg_buffer(mailslot), fields, START_FLAG, END_FLAG, DELIMITER, 1);
        if (get_msg_buffer(mailslot) == inptr) {
            err = 1; // returned same pointer it was sent, means failed parse
        } else { // lookup keyword
            i = 0;
            while ((!found) && (*keywords[i].keyword != 0)) {
                fl = str_len(fields[0]);
                if ((fl >= keywords[i].match_len) &&
                        (fl <= str_len((char*) keywords[i].keyword))) {
                    if (str_match(fields[0], (char*) keywords[i].keyword, fl, 1)) {
                        found = 1;
                        err = (keywords[i].command)(fields); // execute the function
                    }
                }
                i++;
            }
        }
        if ((!found) && (!err)) {
            err = 2;
        }
        if(err) {
            show_err(err);
        }
        if (mailslot == UI) ser_putchar('>');
    }
    return err;
}

void send_command(char* fields[], int num_tokens, char* s_dest_mod) {
#ifdef BUILD_I2C1_MASTER
    int dest = atoi(s_dest_mod); // preserve from destruction by build_frm
    char* str = build_frm(get_msg_buffer(MOSI), fields, num_tokens, START_FLAG, END_FLAG, DELIMITER);
    pwm_timer_set(COMM_PULSE, PULSE_LEN, 0, LED2, NOINVERT);
    writeSI2C1((I2C_SLAVE_BASE_ADR + dest - 1) << 1, str);
    if(get_trace())  printf("Sending to %x: %s\n\r",(I2C_SLAVE_BASE_ADR + dest - 1) << 1, str );
#endif // BUILD_I2C1_MASTER
}

char* parse_frm(char *src, char* vect[], char sf, char ef, char dlm, int trim) {
    char *ptr;
    ptr = src; // ptr is used as a pointer to the current position in input
    int i = 0; // iterates thru the result vector
    int seek_token = 1; // flag to indicate whether seeking token start or delim
    while ((*ptr != sf) && (*ptr != ef) && (*ptr != 0)) ptr++; // scan for start
    if ((*ptr == ef) || (*ptr == 0)) return src; // no start flag found, so give up
    ptr++; // skip over the start flag
    while (*ptr != ef) {
        while ((trim == 1) && (*ptr == ' ')) ptr++; // skip leading spaces
        if (*ptr == 0) return src; // premature end of string
        if (*ptr == dlm) {
            if (seek_token) { // found delim while seeking token, token is empty
                *ptr = '\0'; // insert null to indicate end of string
                vect[i++] = ptr; // save pointer to result vector
            } else {
                *ptr = '\0'; // found delim, so insert null, seek next token
                seek_token = 1;
            }
            } else if (seek_token) { // found start of token, save ptr to result vector
            vect[i++] = ptr;
            seek_token = 0; // token already found, scan til end of token
        }
        ptr++;
    }
    // at this point, the end flag is found, replace it and return pointer
    *ptr = '\0';
    return ptr;
}

char* build_frm(char* results, char* vect[], int cnt, char sf, char ef, char dlm) {
    int i;
    char* orig;
    char cs;
    char msn;
    char lsn;

    orig = results; // remember starting position
    *results = sf;
    results++;
    for (i = 0; i < cnt; i++) { // process each field value
        while (*vect[i] != 0) {
                *results = *vect[i];
                vect[i]++; // next field character
                results++; // next output buffer position
            }
        if (i < (cnt - 1)) { // output a delimiter, but only if not the last field
                *results = dlm;
                results++;
            }
        }
    *results = ef; // output end flag
    cs = calc_cs(orig, sf, ef); // calc checksum
    while (*results != ef) results++; // scan until end flag found
    results++; // bump past end flag
    lsn = i_to_ascii_nib(cs & 0x0f); // get least significant nibble
    msn = i_to_ascii_nib(cs >> 4); // get most significant nibble
    *results = msn; // output most significant nibble of checksum
    results++;
    *results = lsn; // output least significant nibble of checksum
    results++; // next output position
    *results = 0; // put string terminator on end
    return orig; // return pointer to start of frame
}

char* replace_token(char* frame, char* vect[], int n, char* new_token, char sf, char ef, char dlm) {
    char* inptr;
    int count;
    count = token_count(frame, sf, ef, dlm);
    inptr = parse_frm(frame, vect, sf, ef, dlm, 1);
    if (frame != inptr) {
        vect[n] = new_token;
        copy_string(frame, build_frm(get_msg_buffer(GP), vect, count, sf, ef, dlm));
    }
    return frame;
}

char* get_token(char* frame, char* vect[], int n, char* result, char sf, char ef, char dlm) {
    char* inptr;
    copy_string(get_msg_buffer(GP), frame); // copy original
    inptr = parse_frm(get_msg_buffer(GP), vect, sf, ef, dlm, 1);
    if (frame != inptr) copy_string(result, vect[n]);
    return result;
}

int token_count(char* src, char sf, char ef, char dlm) {
    char *ptr;
    int count = 0; // token count
    int seek_token = 1; // flag to indicate whether seeking token start or delim
    ptr = src; // ptr is used as a pointer to the current position in input
    while ((*ptr != sf) && (*ptr != ef) && (*ptr != 0)) ptr++; // scan for start
    if ((*ptr == ef) || (*ptr == 0)) return 0; // no start flag found, so give up
    ptr++; // skip over the start flag
    while (*ptr != ef) {
        if (*ptr == 0) return 0; // premature end of string
        if (*ptr == dlm) {
            if (seek_token) { // found delim while seeking token, token is empty
                count++; // found a token, count it
            } else {
                count++; // found delim, so count another token, seek next token
                seek_token = 1;
            }
            } else if (seek_token) { // found start of token
            seek_token = 0; // token already found, scan til end of token
        }
        ptr++;
    }
    // at this point, the end flag is found, bump token count and return pointer
    count++;
    return count;
}

char calc_cs(char* ptr, char sf, char ef) { // calc xor cs between sf and ef
    char cs = 0;
    while (*ptr != sf) ptr++;
    ptr++; // point past start flag
    while (*ptr != ef) { // xor until end flag found
        cs ^= *ptr;
        ptr++;
    }
    return cs;
}

int validate_cs(char MSB, char LSB, char src_cs) {
    return ((LSB == i_to_ascii_nib(src_cs & 0x0f)) &&
            (MSB == i_to_ascii_nib(src_cs >> 4)));
}

void extract_cs(char* msg, char ef, char* MSB, char* LSB) {
    while (*msg != ef) msg++;
    msg++;
    *MSB = *msg;
    msg++;
    *LSB = *msg;
}

char* assign_msg_buffer(int exchange, char* msg_buffer) {
    mailslots[exchange].mailbox = msg_buffer;
    return mailslots[exchange].mailbox;
}

char* get_msg_buffer(int exchange) {
    return mailslots[exchange].mailbox;
}

int* msg_flg_ptr(int exchange) {
    return (int*)&mailslots[exchange].flg;
}

void set_msg_flag(int exchange) {
    mailslots[exchange].flg = 1;
}

void clr_msg_flag(int exchange) {
    mailslots[exchange].flg = 0;
}

int get_msg_flag(int exchange) {
    return mailslots[exchange].flg;
}

void set_ip_flag(flags n) {
    ip_flag[n] = 1;
}

void clr_ip_flag(flags n) {
    ip_flag[n] = 0;
}

int get_ip_flag(flags n) {
    return ip_flag[n];
}

void init_ip_flags(void) {
    int i;
    for (i = 0; i < NUM_IP_FLAGS; i++) ip_flag[i] = 0;
}


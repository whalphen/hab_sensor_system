/* 
 * File:   i2c_driver.h
 * Author: WHalphen
 *
 * Created on February 17, 2016, 12:10 PM
 */
/*
 * PURPOSE
 * - Provide driver functions for interactions via the I2C digital serial bus
 * - Provide interrupt service routine for handling I2C processing on the slave
 * - Provide Message Available flag for interaction with the main process 
 * The I2C peripheral provides hardware level interfacing to the I2C bus.  This
 * set of functions provide software driver methods and properties.  The functions
 * can be compiled for use on a Master processor or on a Slave processor.  The
 * methods can be compiled to include a second I2C peripheral in the hardware.
 * To compile for use of the I2C1 peripheral as a Master, #define MASTER1
 * To compile for use of the I2C1 peripheral as a Slave, #define SLAVE1
 * To compile for use of the I2C2 peripheral as a Master, #define MASTER2
 * To compile for use of the I2C2 peripheral as a Slave, #define SLAVE2
 * 
 * FUNCTIONS - Master Functions (x represents the peripheral number used)
 * void initI2Cx(int brg); - initializes the I2C master
 *   PARAMETER: brg - value to set the bit rate for the peripheral - the bit rate
 *    is sent directly to the peripheral so should be a value appropriate for the
 *    PIC configuration
 * void startI2Cx(void); - sends Start condition and waits for bus idle
 * void restartI2Cx(void); - sends Restart condition and waits until finished
 * void stopI2Cx(void); - sends Stop condition and waits until finished
 * int putI2Cx(int c); - transmits a character, waits until sent
 *   PARAMETER: character to send
 *   RETURNS: nak/ack bit received from slave
 * char getI2Cx(int acksend); - gets character from bus, responds with nak/ack
 *   PARAMETER: nak/ack bit value to send in response
 *   RETURNS: character received from slave 
 * void write1I2Cx(char addr, char c); - writes one character to slave
 *   PARAMETERS: 
 *     addr - device address of I2C slave
 *     c - character to send to slave
 * void write2I2Cx(char addr, char c1, char c2); - writes two characters to slave
 *   PARAMETERS: 
 *     addr - device address of I2C slave
 *     c1 - first character to send to slave
 *     c2 - second character to send to slave
 * void writeNI2Cx(char addr, char* data, int cnt); - writes N characters to slave
 *   PARAMETERS: 
 *     addr - device address of I2C slave
 *     data - data to send to slave
 *     cnt - count of characters to send
 * void read1I2Cx(char addr, char* c); - reads 1 character from a slave device
 *   PARAMETERS:
 *     addr - device address of I2C slave device
 *     c - pointer to location for character received from slave
 * void read2I2Cx(char addr, char* c1, char* c2);
 *   PARAMETERS:
 *     addr - device address of I2C slave device
 *     c1 - pointer to location for first character received from slave
 *     c2 - pointer to location for second character received from slave
 * void readNI2Cx(char addr, char* data, int cnt); - reads N characters from slave
 *   PARAMETERS:
 *     addr - device address of I2C slave device
 *     data - pointer to location to store the received data
 *     cnt - count of characters to receive from slave device
 * void readSI2Cx(char addr, char* s); - reads string from slave
 *     addr - device address of I2C slave device
 *     s - pointer to location to store the received string
 * void writeSI2Cx(char addr, char* s); - writes string to slave
 *   PARAMETERS: 
 *     addr - device address of I2C slave
 *     s - pointer to string to send to slave
 * 
 * FUNCTIONS - Slave Functions (x represents the peripheral number used)
 *   The Slave functions support only string messages
 * void initI2C1(int brg, char* slave_buffer); - initializes the I2C slave
 *   PARAMETERS:
 *     brg - value to set the bit rate for the peripheral - the bit rate
 *       is sent directly to the peripheral so should be a value appropriate for the
 *       PIC configuration
 *     slave_buffer - pointer to the buffer to be used for sending and
 *       receiving strings
 * int message_avail(void); - flag indicating a string has been received and is
 *   ready to process
 *     RETURNS: 0 for not ready, 1 for ready
 * void clear_message_flg(void); - method to clear the message available flag
 * void __attribute__((interrupt,no_auto_psv)) _SI2C1Interrupt(void) - interrupt
 *   service routine that executes each time a character is received from the 
 *   master processor - this ISR receives the character from the hardware and
 *   puts it in the message buffer - the ISR also gets triggered each time a 
 *   character is sent to the master and handles the sending of the string in 
 *   the message buffer when requested by a read command from the master
 * void set_i2c_buffer(char* slave_buffer); - provides a method for reassigning
 *   the buffer to be used for reception and transmitting - this is useful when
 *   there is risk of a buffer being overwritten by incoming messages from the
 *   master
 * 
 */
#ifndef I2C_DRIVER_H
#define	I2C_DRIVER_H

#include "sys_config.h"
#include "command_processors.h"

#define I2C_ACK 0
#define I2C_NAK 1
#define WADR 0xfe // AND with addr to clear R/W bit
#define RADR 0x01 // OR with addr to set R/W bit

//void set_i2c_buffer(char* slave_buffer);

#ifdef BUILD_I2C1_MASTER
void initI2C1(int brg);
void startI2C1(void);
void restartI2C1(void);
void stopI2C1(void);
int putI2C1(int c);
char getI2C1(int acksend);
void write1I2C1(char addr, char c);
void write2I2C1(char addr, char c1, char c2);
void writeNI2C1(char addr, char* data, int cnt);
void read1I2C1(char addr, char* c);
void read2I2C1(char addr, char* c1, char* c2);    
void readNI2C1(char addr, char* data, int cnt);
void readSI2C1(char addr, char* data);
void writeSI2C1(char addr, char* s);
char getSI2C1(void);

#endif // BUILD_I2C1_MASTER

#ifdef BUILD_I2C2_MASTER

void initI2C2(int brg);
void startI2C2(void);
void restartI2C2(void);
void stopI2C2(void);
int putI2C2(int c);
char getI2C2(int acksend);
void write1I2C2(char addr, char c);
void write2I2C2(char addr, char c1, char c2);
void writeNI2C2(char addr, char* data, int cnt);
void read1I2C2(char addr, char* c);
void read2I2C2(char addr, char* c1, char* c2);    
void readNI2C2(char addr, char* data, int cnt);
void readSI2C2(char addr, char* data);
void writeSI2C2(char addr, char* s);
char getSI2C2(void);

#endif // BUILD_I2C2_MASTER


#ifdef BUILD_I2C1_SLAVE
void initI2C1(int brg, char* mosi_buffer, char* miso_buffer);
int message_avail(void);
void clear_message_flg(void);
char* i2c_slave_buffer(void);
void repoint_misobuf(char* newbuf);
void restore_misobuf(void);

#endif // BUILD_I2C1_SLAVE

int i2c_err(void);
void clr_i2c_err(void);

#ifdef DEBUG
void mst_trigger(int number);
#endif //DEBUG

#endif	/* I2C_DRIVER_H */



#include "serial_terminal.h"

static char* ui_buffer;
static int ui_bidx = 0; // ui buffer index
static int* ui_msg_rdy; // flag to indicate when user has pressed enter to end input line
#ifdef UI_UART2
//Initiation function. Parameter baudratereg determines baud speed
void ser_init(int baudratereg, int* msg_rdy_flg, char* msg_buffer ) {
    U2BRG = baudratereg; //set baud speed
    U2MODE = 0x8000; //turn on module
    U2STA = 0x0400; //set interrupts condition
    IFS1bits.U2RXIF = 0; //reset RX interrupt flag
    IEC1bits.U2RXIE = 1;// enable RX interrupt
    U2STAbits.UTXEN = 1;
    ui_msg_rdy = msg_rdy_flg;
    ui_buffer = msg_buffer;
    ser_putchar(0); 
}

//UART transmit function, parameter c is the character to send
void ser_putchar(char c) {    
    while (U2STAbits.UTXBF); //transmit ONLY if TX buffer is empty
    U2TXREG = c;
}

// Receiver interrupt service routine.  Executes each time a character is
// received by the UART2 peripheral.
void __attribute__((interrupt(auto_psv))) _U2RXInterrupt(void) {
    char c;
    c = U2RXREG;
    if (!*ui_msg_rdy) {
        
        // If the character is a backspace, decrement the buffer index and
        // blank out the last character on the terminal (but only if there is
        // anything in the input buffer).  Check for delete character as some
        // terminals will send this when backspace key is pressed.
        if ((c == BS) || (c == DEL)) { // backspace, so decrement index
            if (ui_bidx) {
                ui_bidx--;
                ser_putchar(BS); // blank out last character
                ser_putchar(SP);
                ser_putchar(BS);
            }
        // If the character is a carriage return, it's the end of the input line
        // so put a string terminator in the buffer, set the message ready flag,
        // and reset the buffer index to the start of the buffer.
        } else if (c == CR) { // CR input so terminate string in buffer
            *ui_msg_rdy = 1; // indicate message ready to process
            ui_buffer[ui_bidx] = 0; // terminate string with a null
            ui_bidx = 0; // reset index to start of buffer
            ser_putchar(CR);
            ser_putchar(LF);
        }
        // If the character is anything else, store it in the buffer and echo
        // it to the terminal.  Do this only if there is enough space left in the 
        // buffer.  If the buffer is full, accept only backspace or CR.
        else if (ui_bidx < (UI_BFR_SIZE - 2)) { // leave space for null
            ui_buffer[ui_bidx++] = c; // store input character to buffer
            ser_putchar(c); // echo to terminal
        }
        else ser_putchar(BEL); // buffer full, only accept BS or CR
    }
    IFS1bits.U2RXIF = 0; //reset interrupt flag
}
#endif // UI_UART2
#ifdef UI_UART1
//Initiation function. Parameter baudratereg determines baud speed
void ser_init(int baudratereg, int* msg_rdy_flg, char* msg_buffer ) {
    U1BRG = baudratereg; //set baud speed
    U1MODE = 0x8000; //turn on module
    U1STA = 0x0400; //set interrupts condition
    IFS0bits.U1RXIF = 0; //reset RX interrupt flag
    IEC0bits.U1RXIE = 1;// enable RX interrupt
    U1STAbits.UTXEN = 1;
    ui_msg_rdy = msg_rdy_flg;
    ui_buffer = msg_buffer;
    ser_putchar(0); 
}

//UART transmit function, parameter c is the character to send
void ser_putchar(char c) {    
    while (U1STAbits.UTXBF); //transmit ONLY if TX buffer is empty
    U1TXREG = c;
}

// Receiver interrupt service routine.  Executes each time a character is
// received by the UART2 peripheral.
void __attribute__((interrupt(auto_psv))) _U1RXInterrupt(void) {
    char c;
    c = U1RXREG;
    if (!*ui_msg_rdy) {
        
        // If the character is a backspace, decrement the buffer index and
        // blank out the last character on the terminal (but only if there is
        // anything in the input buffer).  Check for delete character as some
        // terminals will send this when backspace key is pressed.
        if ((c == BS) || (c == DEL)) { // backspace, so decrement index
            if (ui_bidx) {
                ui_bidx--;
                ser_putchar(BS); // blank out last character
                ser_putchar(SP);
                ser_putchar(BS);
            }
        // If the character is a carriage return, it's the end of the input line
        // so put a string terminator in the buffer, set the message ready flag,
        // and reset the buffer index to the start of the buffer.
        } else if (c == CR) { // CR input so terminate string in buffer
            *ui_msg_rdy = 1; // indicate message ready to process
            ui_buffer[ui_bidx] = 0; // terminate string with a null
            ui_bidx = 0; // reset index to start of buffer
            ser_putchar(CR);
            ser_putchar(LF);
        }
        // If the character is anything else, store it in the buffer and echo
        // it to the terminal.  Do this only if there is enough space left in the 
        // buffer.  If the buffer is full, accept only backspace or CR.
        else if (ui_bidx < (UI_BFR_SIZE - 2)) { // leave space for null
            ui_buffer[ui_bidx++] = c; // store input character to buffer
            ser_putchar(c); // echo to terminal
        }
        else ser_putchar(BEL); // buffer full, only accept BS or CR
    }
    IFS0bits.U1RXIF = 0; //reset interrupt flag
}
#endif // UI_UART1


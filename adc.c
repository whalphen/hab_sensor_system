
#include <p24F32KA302.h>


#define ADC_VOLTAGE 0

void adc_init(void){
    AD1CON1bits.FORM = 0; // unsigned integer result
    AD1CON1bits.SSRC = 7; // auto-convert
    AD1CON1bits.ASAM = 1; // auto-sample   
    AD1CON2bits.PVCFG = 0; // Vdd positive reference
    AD1CON2bits.NVCFG = 0; // Vss negative reference
    AD1CON2bits.BUFREGEN = 1; // result from buffer determined by channel
    AD1CON3bits.ADRC = 0; // clock derived from system clock
    AD1CON3bits.SAMC = 0x1f; // 31 Tad auto-sample time
    AD1CON3bits.ADCS = 0x3f; // 64 x Tcy = Tad
    AD1CHS = ADC_VOLTAGE;
    AD1CSSL = 0; //No scanned inputs
    AD1CON1bits.ADON = 1; // ADC on
}

int adc_get(void) {
    while(!AD1CON1bits.DONE);
    return ADC1BUF0;
}
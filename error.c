
#include "error.h"

int last_err[MAX_MODULES];

// clear error code for specified module, returns cleared error code
int err_clear(int module) { 
    int err = last_err[module];
    last_err[module] = 0;
    return err;
}

// sets error code for specified module if not zero, returns error code
int err_set(int module, int error_code) { 
    last_err[module] = error_code;
    return error_code;
}

// initializes last_err array values to 0
void err_init(void) { 
    int i;
    for (i = 0; i < MAX_MODULES; i++) last_err[i] = 0;
}


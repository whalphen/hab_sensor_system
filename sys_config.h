/* 
 * File:   sys_config.h
 * Author: WHalphen
 * Created on February 29, 2016, 3:41 PM
 * 
 * System configuration values are set here
 */

#ifndef SYS_CONFIG_H
#define	SYS_CONFIG_H

#define DEBUG_MODE 1 // enter 1 for YES or 0 for NO

// system configuration options ************************************************

#define MODULE_ID_CENT 0 // Master processor module: 0
#define MODULE_ID_GPS 1 // GPS module: 1
#define MODULE_ID_LOG 2 // Log module: 2
#define MODULE_ID_PRESS 3 // Pressure module: 3
#define MODULE_ID_TEMP 4 // Temperature module: 4
#define MODULE_ID_LIGHT 5 // Light module: 5
#define MODULE_ID_COMBO 6 // Combination module: 6

// Select configuration
#define CONFIGURATION MODULE_ID_COMBO

//******************************************************************************

#define ID_CENT "0" // Master processor module: 0
#define ID_GPS "1" // GPS module: 1
#define ID_LOG "2" // Log module: 2
#define ID_PRESS "3" // Pressure module: 3
#define ID_TEMP "4" // Temperature module: 4
#define ID_LIGHT "5" // Light module: 5
#define ID_COMBO "6" // Combination module: 6

#if (CONFIGURATION==MODULE_ID_CENT) // Central processor module
    #define MODULE_ID MODULE_ID_CENT
    #define INCLUDE_GPS_MODULE 0 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 0 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 1 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 0 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 0 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 1 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 1// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 1 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_CENT)

#if (CONFIGURATION==MODULE_ID_GPS) // GPS module
    #define MODULE_ID MODULE_ID_GPS
    #define INCLUDE_GPS_MODULE 1 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 0 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 1 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 0 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 0 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 0// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 2 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_GPS)

#if (CONFIGURATION==MODULE_ID_LOG) // Data Log module
    #define MODULE_ID MODULE_ID_LOG
    #define INCLUDE_GPS_MODULE 0 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 1 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 0 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 0 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 1// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 1 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_LOG)

#if (CONFIGURATION==MODULE_ID_PRESS) // Pressure module
    #define MODULE_ID MODULE_ID_PRESS
    #define INCLUDE_GPS_MODULE 0 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 1 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 0 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 1 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 0 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 0 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 1// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 1 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_PRESS)

#if (CONFIGURATION==MODULE_ID_TEMP) // Temperature module
    #define MODULE_ID MODULE_ID_TEMP
    #define INCLUDE_GPS_MODULE 0 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 1 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 0 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 1 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 1 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 0 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 1// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 1 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_TEMP)

#if (CONFIGURATION==MODULE_ID_LIGHT) // Light module
    #define MODULE_ID MODULE_ID_LIGHT
    #define INCLUDE_GPS_MODULE 0 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 0 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 2 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 0 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 1 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 2// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 1 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 1 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 0 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 0 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 1// enter 1 for MASTER, 0 for SLAVE
    #define PCB_MODEL 1 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 8 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_LIGHT)

#if (CONFIGURATION==MODULE_ID_COMBO) // Combined module
    #define MODULE_ID MODULE_ID_COMBO
    #define INCLUDE_GPS_MODULE 1 // enter 1 for YES or 0 for NO
    #define INCLUDE_TEMPERATURE_MODULE 1 // enter 1 for YES or 0 for NO
    #define TEMPERATURE_MODULE_I2C_PORT 1 // enter 1 or 2
    #define INCLUDE_PRESSURE_MODULE 1 // enter 1 for YES or 0 for NO
    #define PRESSURE_MODULE_I2C_PORT 1 // enter 1 or 2
    #define INCLUDE_EEPROM_MODULE 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_1 1 // enter 1 for YES or 0 for NO
    #define USE_EEPROM_2 0 // enter 1 for YES or 0 for NO
    #define EEPROM_I2C_PORT 1// enter 1 or 2
    #define INCLUDE_LIGHT_MODULE 0 // enter 1 for YES or 0 for NO
    #define LIGHT_MODULE_I2C_PORT 2 // enter 1 or 2 *** currently use only 2 ***
    #define INCLUDE_CENTRAL_PROCESSOR 0 // 1:YES 0:NO -- central processor functions
    #define INCLUDE_SLAVE_PROCESSOR 0 // 1:YES 0:NO -- slave processor functions
    #define INCLUDE_VOLTMETER 1 // enter 1 for YES or 0 for NO
    #define I2C1_ROLE 1 // enter 1 for MASTER, 0 for SLAVE
    #define I2C2_ROLE 0// enter 1 for MASTER, 0 for SLAVE or unused
    #define PCB_MODEL 2 // 2:SENSOR_MODULE_2(GPS), 1:SENSOR_MODULE_1, 0:DIGITAL_XCVR
    #define CLOCK_SOURCE 1 // enter 1 for INTERNAL or 0 for EXTERNAL
    #define CLOCK 4 // enter oscillator frequency in MHz: 32|16|8|4
    #define p24f32KA302 // CPU
#endif // (CONFIGURATION==MODULE_ID_COMBO)

#define I2C_SLAVE_BASE_ADR 0x30

#ifdef p24f32KA302
    #include <p24f32KA302.h>
#endif // p24f32KA302

#if (CLOCK_SOURCE==1)
    #define INT_OSC // Internal oscillator
#endif
#if (CLOCK_SOURCE==0)
    #define EXT_OSC // External oscillator
#endif

#if (PCB_MODEL==0)
    #define DIGITAL_XCVR // Using DIGITAL_XCVR board
    #define GPS_UART2 // GPS to use UART2
    #define UI_UART 1 // user interface to use UART1
    #define UI_UART1 // user interface to use UART2
#endif
#if (PCB_MODEL==1)
    #define SENSOR_MODULE_1 // Using SENSOR_MODULE_1 board
    #define UI_UART 2 // user interface to use UART2
    #define UI_UART2 // user interface to use UART2
#endif
#if (PCB_MODEL==2)
    #define SENSOR_MODULE_2 // Using SENSOR_MODULE_2 board
    #define GPS_UART1 // GPS to use UART1
    #define UI_UART2 // user interface to use UART2
    #define UI_UART 2 // user interface to use UART2
#endif

#if (INCLUDE_GPS_MODULE==1)
    #define BUILD_GPS // build GPS module components
#endif
#if ((INCLUDE_GPS_MODULE==1) && (PCB_MODEL==2))
    #define BUILD_PPS // pulse per second clock using gps pps signal
#endif
#if (INCLUDE_TEMPERATURE_MODULE==1)
    #define BUILD_TEMPERATURE // build temperature module components
#endif
#if (TEMPERATURE_MODULE_I2C_PORT==1)
    #define TEMPERATURE_MODULE_I2C1
#endif
#if (TEMPERATURE_MODULE_I2C_PORT==2)
    #define TEMPERATURE_MODULE_I2C2
#endif
#if (INCLUDE_PRESSURE_MODULE==1)
    #define BUILD_PRESSURE // build pressure module components
#endif
#if (PRESSURE_MODULE_I2C_PORT==1)
    #define PRESSURE_MODULE_I2C1
#endif
#if (PRESSURE_MODULE_I2C_PORT==2)
    #define PRESSURE_MODULE_I2C2
#endif
#if (INCLUDE_EEPROM_MODULE==1)
    #define BUILD_EEPROM // build eeprom module components
#endif
#if (EEPROM_I2C_PORT==1)
    #define EEPROM_MODULE_I2C1
#endif
#if (EEPROM_I2C_PORT==2)
    #define EEPROM_MODULE_I2C2
#endif
#if (INCLUDE_LIGHT_MODULE==1)
    #define BUILD_LIGHT // build light module components
#endif
#if (LIGHT_MODULE_I2C_PORT==1)
    #define LIGHT_MODULE_I2C1
#endif
#if (LIGHT_MODULE_I2C_PORT==2)
    #define LIGHT_MODULE_I2C2
#endif
#if (INCLUDE_VOLTMETER==1)
    #define BUILD_VOLTMETER
#endif
#if (INCLUDE_CENTRAL_PROCESSOR==1)
    #define BUILD_CENTRAL_PROCESSOR // build central processor components
    #define IS_MASTER 1 // Master processor
#endif
#if (INCLUDE_SLAVE_PROCESSOR==1)
    #define BUILD_SLAVE_PROCESSOR // build slave processor components
    #define IS_MASTER 0 // Slave processor
#endif
#if ((INCLUDE_CENTRAL_PROCESSOR==0) && (INCLUDE_SLAVE_PROCESSOR==0))
    #define IS_MASTER 0
#endif
#if (I2C1_ROLE==1)
    #define BUILD_I2C1_MASTER // Master on I2C bus 1
#endif
#if (I2C1_ROLE==0)
    #define BUILD_I2C1_SLAVE // Slave on I2C bus 1
#endif
#if (I2C2_ROLE==1)
    #define BUILD_I2C2_MASTER // Master on I2C bus 2
#endif
#if (I2C2_ROLE==0)
    #define BUILD_I2C2_SLAVE // Slave on I2C bus 2
#endif

// clock and rate definitions
#if (CLOCK==32)
    #define CK32MHZ
    // baud rate definitions for 32MHz clock
    #define BAUD_1200 832
    #define BAUD_9600 103
    #define BAUD_19200 51
    #define BAUD_115200 8
    #define BRG 37 // I2C clock 400 kHz
    #define PROC_TICKS_PER_COUNT 16000 // timebase counter value
#endif
#if (CLOCK==16)
    #define CK16MHZ
    // baud rate definitions for 16MHz clock
    #define BAUD_1200 416
    #define BAUD_9600 51
    #define BAUD_19200 25
    #define BRG 18 // I2C clock 400 kHz
    #define PROC_TICKS_PER_COUNT 8000 // timebase counter value
#endif
#if (CLOCK==8)
    #define CK8MHZ
    // baud rate definitions for 8MHz clock
    #define BAUD_1200 207
    #define BAUD_9600 25
    #define BAUD_19200 12
    #define BRG 9 // I2C clock 400 kHz
    #define PROC_TICKS_PER_COUNT 4000 // timebase counter value
#endif
#if (CLOCK==4)
    #define CK4MHZ
    // baud rate definitions for 4MHz clock
    #define BAUD_1200 103
    #define BAUD_9600 12
    #define BRG 19 // I2C clock 100 kHz
    #define PROC_TICKS_PER_COUNT 2000 // timebase counter value
#endif
#define CLOCK_ADJUST_INCR (PROC_TICKS_PER_COUNT / 2000)

#define ON 1
#define OFF 0

#define MAX_MODULES 8
typedef enum {MAIN, DATA_LOG, GPS_MOD, TEMP, PRES, LIGHT} module;

#if (DEBUG_MODE==1)
    #define DEBUG
#endif

#ifdef SENSOR_MODULE_1
    // link PWM timer to physical pin
    #define DOUT1 LATAbits.LATA3 //digital output 1
    #define DOUT2 LATBbits.LATB14 //digital output 2
    #define DOUT3 LATAbits.LATA3 //digital output 3
    #define DOUT4 LATBbits.LATB7 //digital output 4
    #define DOUT5 LATAbits.LATA7 //digital output 5
    #define LED1 4 //digital output 4 is LED1
    #define LED_LT 4
    #define LED_GRN 4
    #define ALARM 3 // digital output 3 is ALARM
    #define DO1 3 // digital output 3 is DO1, open collector driver
    #define LED2 5 //digital output 5 is LED2
    #define LED_RT 5
    #define LED_RED 5
    #define INT 2 // digital output 2 is INT output
#endif // SENSOR_MODULE_1
#ifdef SENSOR_MODULE_2
    // link PWM timer to physical pin
    #define DOUT1 LATAbits.LATA3 //digital output 1
    #define DOUT2 LATBbits.LATB10 //digital output 2
    #define DOUT3 LATAbits.LATA3 //digital output 3
    #define DOUT4 LATAbits.LATA7 //digital output 4
    #define DOUT5 LATBbits.LATB3 //digital output 5
    #define LED1 4 //digital output 4 is LED1
    #define LED_LT 4
    #define LED_GRN 4
    #define ALARM 3 // digital output 3 is ALARM
    #define DO1 3 // digital output 3 is DO1, open collector driver
    #define LED2 5 //digital output 5 is LED2
    #define LED_RT 5
    #define LED_RED 5
    #define INT 2 // digital output 2 is INT output
#endif // SENSOR_MODULE_2
#ifdef DIGITAL_XCVR
    // link PWM timer to physical pin
    #define DOUT1 LATAbits.LATA1 //digital output 1
    #define DOUT2 LATAbits.LATA0 //digital output 2
    #define DOUT3 LATBbits.LATB4 //digital output 3
    #define DOUT4 LATBbits.LATB4 //digital output 4
    #define DOUT5 LATBbits.LATB4 //digital output 5
    #define DO1 1 // digital output 1 is DO1, open collector driver
    #define DO2 2 // digital output 2 is DO2, open collector driver
    #define GPIO 3 //digital output 3 is GPIO RB4
    #define ALARM 3 // digital output 3 is ALARM
#endif // DIGITAL_XCVR

#define SWITCH_INPUT PORTBbits.RB4

#define MATCH_CASE 1 // don't ignore case in string functions
#define IGNORE_CASE 0 // ignore case in string functions

#define INVERT 1 // invert output of timer
#define NOINVERT 0 // non-inverted output of timer

#define SI1133_DELAY 800 // delay 600 ms before reading response
#define TEMP_DELAY 5
#define PRESS_DELAY 50
#define NVM_DELAY 50
#define LOG_DELAY 10
#define GPS_DELAY 50
#define ACK_DELAY 100

#define NUM_TIMERS 8 // pwm timers
#define HEARTBEAT 4 // heartbeat timer for module
#define COMM_PULSE 0 // used to pulse red led when a master/ slave interaction
#define ERASE_LOG 3 // used during manual log erase
#define ALARM_TIMER 6 // used for audio alarm
#define DELAY_TIMER 7 // used for delay in main loop
#define PULSE_LEN 100 // length of interaction pulse in ms
#define ERROR 1 // pwm timer used to flash error signal
#define ERR_PD 25 // error signal counter value
#define ALM_ON 1000 // audio beacon on for 1 second
#define ALM_OFF 4000 // audio beacon off for 4 seconds
#define ALT_THRESHOLD 1000 // turn off unnecessary items at this altitude (m)
#define NUM_CYCLE_TIMERS 4 // cycle timers
#define LONG 1 // long cycle
#define MED 2 // medium cycle
#define SHORT 3 // short cycle
#define SPARE 4 // spare cycle timer
#define CYCLE0_DEFAULT "60000" // default cycle time of 60000 ms (60 seconds)
#define CYCLE1_DEFAULT "30000"
#define CYCLE2_DEFAULT "1000"
#define CYCLE3_DEFAULT "0"
#define TICKS_DEFAULT "0"
#define OSC_TUN_DEFAULT "0"


// inter process messaging
#define NUM_MAILSLOTS 6
#define MAX_TOKENS 24
typedef enum {MOSI, MISO, UI, GPS, LOG, GP} exchanges;
#define MOSI_BFR_SIZE 128
#define MISO_BFR_SIZE 128
#define UI_BFR_SIZE 80
#define GPS_BFR_SIZE 128
#define LOG_BFR_SIZE 128
#define GP_BFR_SIZE 128 // general purpose buffer
#define SCRATCHPAD_SIZE 128

#define RECORD_FIELDS 17
#define LOG_PAGE_SIZE 128
#define LOG_START 0
#define EEPROM_BASE_ADDR 0xA0
#define LOG_END "$<<END>>*4F"
#define NVM_BYTES 512
#define NVM_PAGE_SIZE 128
#define NVM_PAGES (NVM_BYTES / NVM_PAGE_SIZE)
#define NVM_CONFIG_PAGE 0

#if (USE_EEPROM_1==0) // first chip not installed, use addr for second chip
    #define EEPROM_START_ADDR 0x60000
#else
    #define EEPROM_START_ADDR 0
#endif

#if ((USE_EEPROM_1)==1 && (USE_EEPROM_2==1))
    #define EEPROM_PAGES 2048
#else
    #define EEPROM_PAGES 1024
#endif

// temperature sensor definitions
#define AMBIENT 0
#define HYSTERESIS 2
#define CONFIG 1
#define RES0625 0x60
#define RES1250 0x40
#define RES2500 0x20
#define RES5000 0x00
#define SENSOR1 0x4d // device address for sensor1


#define MAX_FIELDS 21 // max number of fields in a message

// general purpose inter-process flags
#define NUM_IP_FLAGS 8
typedef enum {IPF_0, IPF_1, IPF_2, IPF_3, IPF_4, IPF_5, IPF_6, IPF_DIG_OUT_DISABLE} flags;

// digital output modes
typedef enum {PULSE, PWM, DO_ON, DO_OFF} dout_modes;
    
#define START_FLAG '$'
#define END_FLAG '*'
#define DELIMITER ','

#define KEYWORD_MAX_LEN 8

#define LIGHT_SENSOR (0x55 << 1)
#define SI1133_PART_ID 0x00 // part id register address
#define SI1133_HW_ID 0x01 // hardware id register address
#define SI1133_REV_ID 0x02 // revision id register address
#define SI1133_RESET_CMD_CTR 0x00
#define SI1133_RESET_SW 0x01
#define SI1133_FORCE 0x11
#define SI1133_INPUT0 0x0a
#define SI1133_CMD_REG 0x0b
#define SI1133_RESPONSE0_REG 0x11
#define SI1133_IRQ_STATUS_REG 0x12
#define SI1133_RESPONSE1_REG 0x10
#define SI1133_CHAN_LIST 0x01
#define ADCCONFIG 0 // address offset
#define ADCSENS 1 // address offset
#define ADCPOST 2 // address offset
#define MEASCONFIG 3 // address offset
#define SI1133_PARAM_CNT 4
#define SI1133_CHANNEL_CNT 6
#define SI1133_RESULT_FIELD_SIZE 9
#define SI1133_SMALL_IR 0
#define SI1133_MEDIUM_IR 1
#define SI1133_LARGE_IR 2
#define SI1133_WHITE 0x0b
#define SI1133_LARGE_WHITE 0x0d
#define SI1133_UV 0x18
#define SI1133_UV_DEEP 0x19
#define SI1133_RATE_1024 0 // normal measurement time
#define SI1133_RATE_2048 0b00100000
#define SI1133_RATE_4096 0b01000000
#define SI1133_RATE_512 0b01100000 // very short measurement time
#define SI1133_RANGE_NORMAL 0
#define SI1133_RANGE_HIGH 0b10000000
#define SI1133_16BIT 0
#define SI1133_24BIT 0b01000000
#define SI1133_SW_GAIN_1 0b00000000
#define SI1133_SW_GAIN_2 0b00010000
#define SI1133_SW_GAIN_4 0b00100000
#define SI1133_SW_GAIN_8 0b00110000
#define SI1133_SW_GAIN_16 0b01000000
#define SI1133_SW_GAIN_32 0b010100000
#define SI1133_SW_GAIN_64 0b01100000
#define SI1133_SW_GAIN_128 0b01110000
#define SI1133_HW_GAIN_1 1
#define SI1133_HW_GAIN_2 2
#define SI1133_HW_GAIN_4 3
#define SI1133_HW_GAIN_8 4
#define SI1133_HW_GAIN_16 5
#define SI1133_HW_GAIN_32 6
#define SI1133_HW_GAIN_64 7
#define SI1133_HW_GAIN_128 8
#define SI1133_HW_GAIN_256 9
#define SI1133_HW_GAIN_512 10
#define SI1133_HW_GAIN_1024 11
#define SI1133_HW_GAIN_2048 12

#define CONFIG_CNT 6
#define MAX_CONFIG_VALUE_LEN 10
typedef enum {PG_TYPE, CYCLE0, CYCLE1, CYCLE2, CYCLE3, TICKS, OSC_TUN} configs;

#endif	/* SYS_CONFIG_H */


/* 
 * File:   command_processors.h
 * Author: WHalphen
 *
 * Created on March 8, 2016, 8:54 AM
 * 
 * Command Processors
 * Collection of functions to process commands
 * 
 * void soft_reset(void);
 *    purpose: does a software reset of the processor, causing it to restart
 *    its program
 * void sleep(void);
 *    purpose: shuts down the processor - requires a hardware reset to start up
 *    again
 * void show_err(int err_num);
 *    purpose: displays a message for the specified error code
 *    parameter: err_num is the error code to be reported
 * void start_gps_measurement(void);
 *    purpose: begins the gps measurement process by signaling the gps to report
 * void retrieve_gps_measurement(void); 
 *    purpose: retrieves the results from the gps and puts them in the message
 *    buffer
 * void start_temperature_measurement(void);
 *    purpose: begins the temperature measurement process by signaling the
 *    sensor to report a measurement
 * void retrieve_temperature_measurement(void);
 *    purpose: retrieves the results from the temperature sensor
 * void dout(int do_num, dout_modes mode, int on, int off, int invert);
 *    purpose: controls a digital output by turning it on, off, sending a pulse,
 *    or sending a PWM series to a specified output
 *    parameters: 
 *       do_num - index of the digital output to be used
 *       mode - indicates output mode: PULSE, PWM, DO_ON, DO_OFF
 *          PULSE outputs a times one-shot pules
 *          PWM outputs a timed pulse width modulated ongoing sequence
 *          DO_ON turns an output on (high)
 *          DO_OFF turns an output off (low)
 *       on - number of milliseconds for the output to be on (PULSE and PWM modes)
 *       off - number of milliseconds for the output to be off (PWM mode)
 *       invert - if 1, the output is inverted (PULSE and PWM modes)
 *          if 0, the output is not inverted 
 */

#ifndef COMMAND_PROCESSORS_H
#define	COMMAND_PROCESSORS_H

#include <stdio.h>
#include <stdlib.h>
#include "i2c_driver.h"
#include "msg_processor.h"
#include "timebase.h"
#include "gps.h"
#include "adc.h"
#include "Si1133_driver.h"

int soft_reset(char* fields[]);
int sleep(char* fields[]);
void show_err(int err_num);
int audio_on(char* fields[]);
int audio_off(char* fields[]);
int gps_test(char* fields[]);
int initiate_measurement(char* fields[]);
int get_results(char* fields[]);
int pressure(char* fields[]);
void show_err(int err_num);
int dump_log(char* fields[]);
int prep_log(char* fields[]);
int log_entry(char* fields[]);
int status_log(char* fields[]);
int save_config(char* fields[]);
int led_test(char* fields[]);
int switch_test(char* fields[]);
int temperature(char* fields[]);
int run_cycle(char* fields[]);
int pause_cycle(char* fields[]);
int config_cycle(char* fields[]);
int config_ticks(char* fields[]);
int config_osctun(char* fields[]);
int vbat(char* fields[]);
int write_nvm(char* fields[]);
int read_nvm(char* fields[]);
int light(char* fields[]);
int get_page(char* fields[]);
int acknowledge(char* fields[]);
int trace_msg(char* fields[]);
int pwm(char* fields[]);
int ip_flag_msg(char* fields[]);
int debugger(char* fields[]);

#endif	/* COMMAND_PROCESSORS_H */


/* 
 * File:   utilities.h
 * Author: WHalphen
 *
 * Created on February 26, 2016, 10:05 AM
 
 * FUNCTION 
 *   char ascii_nib_to_i(char nibble); 
 *   utility for converting ascii nibble to an integer 0-F
 *   PARAMETER: nibble is the ascii character value to be converted
 *   RETURNS: integer value 0-F
 * FUNCTION
 *   char i_to_ascii_nib(char nibble);
 *   utility for converting integer 0-F to ascii nibble
 *   PARAMETER: nibble is the integer value to be converted
 *   RETURNS: ascii nibble
 * FUNCTION 
 *   int str_match(char* str1, char *str2, int len);
 *   utility for comparing two strings
 *   PARAMETERS
 *     str1 -- pointer to first string to be compared
 *     str2 -- pointer to second string to be compared
 *     len - number of characters to be compared (from start of string)
 *     ignore_case - if not zero, the comparison will not be case sensitive
 *        if 0, comparison will be case sensitive
 * FUNCTION
 *    char ucase(char c); 
 *    converts a character to upper case
 *    PARAMETERS
 *       c -- character to be converted
 *    RETURNS
 *       upper case character 
 * FUNCTION
 *    void copy_string(char* dest, char* src);
 *    copies a string to a buffer
 *    PARAMETERS
 *       dest -- pointer to the destination buffer
 *       src -- pointer to the string to be copied
 * 
 * Error management functions
 * FUNCTION
 *    int err_clear(int module); 
 *    clear error code for specified module, 
 *    returns the value of the error code before it is cleared
 * FUNCTION
 *    int err_set(int module, int error_code);
 *    sets error code for the specified module if error_code is not zero
 * FUNCTION
 *    void err_init(void);
 *    initializes last_err array values to 0
*/


#ifndef UTILITIES_H
#define	UTILITIES_H

#include "sys_config.h"

int str_len(char* s);
char ascii_nib_to_i(char nibble);
char i_to_ascii_nib(char nibble);
int str_match(char *str1, char *str2, int len, int ignore_case);
char ucase(char c);
char* copy_string(char* dest, char* src);
void wdt(int on_off);
void wdt_clr(void);
int err_clear(int module);
int err_set(int module, int error_code);
void err_init(void);
void set_trace(int state);
int get_trace(void);
void set_debug(int state);
int get_debug(void);

#endif	/* UTILITIES_H */

/* 
 * File:   hab_main.h
 * Author: WHalphen
 *
 * Created on December 17, 2015, 11:59 AM
 * 
 * Main process
 */

#ifndef HAB_MAIN_H
#define	HAB_MAIN_H

#include "sys_config.h"
#include <stdio.h>
#include <libpic30.h>
#include <xc.h>
#include "setup.h"
#include "serial_terminal.h"
#include "i2c_driver.h"    
#include "timebase.h"
#include "gps.h"
#include "thermo.h"
#include "utilities.h"
#include "pressure.h"
#include "eeprom.h"
#include "Si1133_driver.h"

#endif	/* HAB_MAIN_H */


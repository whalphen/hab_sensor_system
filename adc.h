/* 
 * File:   adc.h
 * Author: WHalphen
 *
 * Created on May 4, 2016, 8:12 PM
 */

#include "sys_config.h"

#ifndef ADC_H
#define	ADC_H

void adc_init(void);
int adc_get(void);

#endif	/* ADC_H */


/* 
 * File:   timebase.h
 * Author: WHalphen
 *
 * Created on January 4, 2016, 11:18 AM
 */

#ifndef TIMEBASE_H
#define	TIMEBASE_H

#include "sys_config.h"
#include <stdio.h>

//int buf_overflow = 0;

/* Routines for a timebase for the PIC24FxxK30x processor
 * 
 * One timer is used to count system clock ticks.  Every 0.1 millisecond, the
 * counter is reloaded and an interrupt is used to update a millisecond counter.
 * 
 * Uses Timer 2 and interrupt.
 * 
 * Use tb_initialize(count) to create and start the timebase.  'count' is the
 * number of instruction cycles per 0.01 millisecond for the current processor
 * configuration.   (Fosc = 32 MHz, resulting in instruction clock of 16 MHz.)
 * The tb_count() property returns the number of tenth millisecond counts
 * since the initialization of the timebase, or since the tb_clear_counter()
 * function was last executed.  The tb_count() property value maxes out and
 * rolls over to zero after 2^32 counts.
 * 
 * The following functions are available:
 * tb_start() -- sets up and starts the timer, zeroes the counter
 * tb_stop() -- stops the timer
 * tb_time_ms() -- returns the number of milliseconds since timer initialized
 *   (valid for 2^32/10 milliseconds (~4.97 days) from initialization)
 * tb_delay_ms(milliseconds) -- delays for specified number of milliseconds
 * tb_clear_counter() -- clears tenths of milliseconds_counter
 * tb_initialize(count) -- initializes and starts timebase, and counter
 * tb_count() -- returns tenths of milliseconds since timer initiallized
 * void pwm_timer_set(int tmr_num, int on, int off, int dig_out, int invert);
 *    sets specified timer for one-shot or pwm output
 *    parameters:
 *       tmr_num -- specified timer to be used
 *       on -- on time in milliseconds
 *       off -- in the case of PWM, off time in milliseconds - if 0, one-shot timer
 *       dig_out -- selects digital output to be driven by the timer - if 0, no dig output
 *       invert -- in the case of digital output, 1 will cause output to be inverted
 * void pwm_timer_reset(int n); -- immediately stops and resets the specified timer
 * int pwm_timer(int n); -- returns the state of the specified timer
 * void init_pwm_timers(void); -- initializes the timers -- execute once during startup
 * 
 * If a 1 Hz pulse from a gps is connected to INT1 input, the following functions
 * can be used:
 * void pps_init(void); // initializes Pulse Per Second (pps) functionality
 * void __attribute__((__interrupt__)) _INT1Interrupt(void); // ISR for INT1
 * int pps_seconds(void); // returns number of seconds counted by pps counter
 * 
 * The cycle timer is intended to provide a periodic reference so the main
 * process can execute certain functions on a periodic basis.  Once set, the
 * timer will countdown and set an alarm flag when the countdown reaches zero.
 * The countdown timer is reloaded and the process repeats.
 * The flag can be reset by the main process.  It is then set again when the
 * next timing cycle expires.
 * 
 * void init_cycle_timers(void); initializes cycle timers
 * void cycle_timer_set(int timer, unsigned long time, int alarm);
 *   sets cycle timer indicated by 'timer' to a timer period of 'time' milliseconds 
 *   set 'alarm' to 1 if alarm is to be initially on or 0 to start with it off
 * int cycle_timer_check(int timer); returns 1 if alarm has been triggered,
 *   returns 0 if the alarm has not yet been triggered for the current cycle
 * void cycle_clock_pause(void);
 *   pauses cycle clock, but does not clear countdown timer value
 * void cycle_clock_run(void);
 *   runs cycle clock, decrementing the value in the countdown timer
 * void cycle_clock_enable(int enable);
 *   enables the cycle clock when the value of 'enable' is 1
 *   disables the cycle clock when the value of 'enable' is 0
 * 
 */

// start timer
void tb_start(void);

// stop timer
void tb_stop(void);  

// initialize and start the timebase
void tb_initialize(unsigned int count);

// get counts since timer initialized
unsigned long tb_count();

void tb_delay_ms(unsigned int milliseconds);     

void tb_clear_counter();

void __attribute__((__interrupt__)) _T2Interrupt(void);

void pwm_timer_set(int tmr_num, int on, int off, int dig_out, int invert);
void pwm_timer_reset(int n);
int pwm_timer(int n); // returns state of timer n
void init_pwm_timers(void);

void pps_init(void);
void __attribute__((__interrupt__)) _INT1Interrupt(void);
int pps_seconds(void);

void init_cycle_timers(void);
void cycle_timer_set(int timer, unsigned long time, int alarm);
int cycle_timer_check(int timer);
void cycle_clock_pause(void);
void cycle_clock_run(void);
void cycle_clock_enable(int enable);

int sw_get_press_len(void);
void sw_clr_press(void);
int sw_status(void);

unsigned long tb_seconds(void);
void tb_clr_seconds(void);

#endif	/* TIMEBASE_H */


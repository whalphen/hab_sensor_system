/* 
 * File:   Si1133_driver.h
 * Author: WHalphen
 *
 * Created on March 23, 2016, 1:14 PM
 */

/*
 * Functions for interacting with Si1133 light sensor.
 * 
 * void si1133_reset_cmd_ctr(void);
 *    resets the command counter
 * 
 * char si1133_response0(void);
 *    checks for operation completion and for errors
 * 
 * char si1133_param_get(char param_num);
 *    reads parameter xxxxxx and returns result
 * 
 * void si1133_param_set(char param, char value);
 *    writes 'value' to parameter 'param_num'
 * 
 */

#ifndef SI1133_DRIVER_H
#define	SI1133_DRIVER_H
#include "sys_config.h"
#ifdef BUILD_LIGHT
#include "i2c_driver.h"

void si1133_reset_cmd_ctr(void);
int si1133_reset(void);
int si1133_param_set(char param, char value);
unsigned char si1133_param_get(char param);
void si1133_channel_select(int channel, int on);
void si1133_setup_channels(void);
unsigned char si1133_read_register(char register_address);
void si1133_write_register(char register_address, char value);
void si1133_force(void);
unsigned char si1133_response0(void);
unsigned char si1133_response1(void);
void si1133_reset_sw(void);
void si1133_sensor_config(int channel, char adcconfig, char adcsens, char adcpost, char measconfig);
unsigned long si1133_results(int channel);
void dump_params(void);
void dump_registers(void);
#endif // BUILD_LIGHT
#endif	/* SI1133_DRIVER_H */

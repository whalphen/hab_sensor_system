/* 
 * File:   msg_processor.h
 * Author: WHalphen
 *
 * Created on January 20, 2016, 6:48 PM
 * 
 * Functions to interpret and execute command messages
 * When a message has been received, the functions in this module can be used
 * to process the message and execute the command.  Processing of a message
 * includes the following activities:
 * Break up the message into tokens
 * The first token will indicate the type of message and subsequent processing
 * Remaining tokens are handled according to the message type
 * Error codes are returned when problems occur
 * 
 * FUNCTION
 * char* parse_frm(char *src, int* (*vect[]), char sf, char ef, char dlm, int trim);
 * Divides a delimited sequence of characters in a message frame into a set of 
 * string tokens.  
 * The tokenizing starts when it finds a start flag and ends when it finds an
 * end flag.
 * A pointer to the sequence of characters is passed to the function in the
 * 'src' parameter.  The function returns a pointer to the end flag position
 * when done. (After replacing the end flag with a null character.)
 * Each token is converted to a string by replacing its following delimiter
 * with a '\0' value as a string terminator.
 * A pointer to a vector (of pointers) is provided in the parameter 'vect' and
 * is populated with pointers to each token by the function.  It is up to the
 * user to ensure that the vector is large enough to contain all the token
 * pointers and to ensure that a stop flag exists.  If a start flag is not found
 * before an end flag or end string is encountered, no tokenizing will be done and the 
 * function will return a pointer to the beginning of the sequence of 
 * characters.
 * The start flag is not included in the first token of the response vector.
 * If the 'trim' parameter is 1, leading spaces will be removed from the tokens.
 * PARAMETERS
 * char *src -- A pointer to the sequence of characters to be parsed
 * char *vect[] -- A pointer to an array to hold the pointers to the fields
 *              returned by the function.  The function divides the fields into
 *              individual strings and populates this array with a pointer to
 *              each of those strings in the order they were received.
 * char sf -- The start flag used in the frame
 * char ef -- The end flag used in the frame
 * char dlm -- The delimiter used in the frame
 * int trim -- Set to 1 to trim leading spaces from each field, other values will
 *             leave the leading spaces
 * RETURN 
 * Normally the function returns a pointer to the end flag position when done (after
 * replacing the end flag with a null character).  If no start flag was found, 
 * the function returns a pointer the the start of the character sequence.
 * EXCEPTIONS
 * If no start flag was found, or end of string was found prematurely,
 * the function returns a pointer the the start of the character sequence.
 * 
 * FUNCTION
 * void build_frm(char* results, char* vect[], int cnt, char sf, char ef, char dlm);
 * This function assembles a message frame from strings.  The message frame is
 * assembled beginning with a start flag which is followed by a variable number
 * of field values separated by delimiter.  This is followed by an end flag.
 * The field values are passed to the function as an array of pointers to the
 * strings to be added to the message frame.  The strings will be added as
 * fields in the order they are listed in the array.
 * PARAMETERS
 * char* results - pointer to a buffer to hold the resulting string
 * char* vect[] - array holding pointers to the field values to be included -
 *   the pointers should be in the order in which the values are to be appended
 * int cnt - count of field values to be appended
 * char sf - start flag to be used
 * char ef - end flag to be used
 * char dlm - delimiter to be used 
 * RETURNS: pointer to start of string that was built
 * 
 * FUNCTION
 * char calc_cs(char* ptr, char sf, char ef);
 *    calculates the exclusive-or checksum for a message frame
 * PARAMETERS
 *    ptr - pointer to the start of the message frame
 *    sf - character used for the start flag
 *    ef - character used for the end flag
 * RETURNS
 *    returns a single byte checksum value
 * 
 * FUNCTION
 * int validate_cs(char MSB, char LSB, char src_cs);
 *    compares an ascii encoded integer, provided as MSB and LSB characters
 *    to an integer - returns a 0 if no match, returns non-zero if match
 * PARAMETERS
 *    MSB - most significant byte of ascii encoded integer
 *    LSB - least significant byte of ascii encoded integer
 *    src_cs - integer to be compared
 * RETURNS
 *    returns a zero if no match, returns non-zero if match
 * 
 * FUNCTION
 * void extract_cs(char* msg, char ef, char* MSB, char* LSB);
 *    extracts the checksum value included in a message frame - checksum is
 *    expected to immediately follow the end flag
 * PARAMETERS
 *    msg - pointer to the start of the message
 *    ef - end flag used
 *    MSB - pointer to a location to return the MSB character of the checksum
 *    LSB - pointer to a location to return the LSB character of the checksum
 * 
 * FUNCTION
 * int process_msg(char* fields[], exchanges mailslot);
 *    processes the message in the mailbox indicated by the mailslot parameter
 *    the message is parsed and the fields are stored in the fields array
 *    the message is interpreted and the corresponding command executed
 * PARAMETERS
 *    fields[] - pointer to the array to hold the pointers to the field strings
 *    parsed from the message
 *    mailslot - index to the mailbox used
 * 
 * FUNCTION
 * char** get_token_ptrs(void);
 *    returns a pointer to the array of pointers indicating the addresses of the
 *    tokens (fields) parsed from a message or to be included in a message to be
 *    built
 * 
 * FUNCTION
 * void add_token_ptr(int position, char* pointer);
 *    adds a token (field) to a list of fields to be included in a message to be
 *    built
 * PARAMETERS
 *    position - position in the list at which the token is to be added
 *    pointer - pointer to the token to be added
 * 
 * FUNCTION
 * void send_command(char* fields[], int num_tokens, char* s_dest_mod);
 *    sends a command message to the specified destination module
 * PARAMETERS
 *    fields[] - pointer to the array of fields to be included in the message
 *    num_tokens - number of tokens (fields) to be included in the message
 *    s_dest_mod - pointer to a character representing the slave module to which
 *    the message is to be sent - this would typically be pointed to the ascii
 *    character in a supplied message indicating the intended destination of 
 *    the message or command
 * 
 * Following are structures and functions for handling the movement of
 * messages between the master, slave, user interface, and gps.
 * A postal structure consists of a mailbox and a message flag.  When a message
 * is received, it is placed into the mailbox by the sender and the flag is 
 * set to 1 to indicate a message is ready to be processed. When the receiving
 * process retrieves the message, it sets the flag to 0.
 * 
 * char* assign_msg_buffer(int exchange, char* msg_buffer);
 *    purpose: Assigns a memory buffer to a mailbox.
 *    parameters: 
 *       exchange - the index of the mailbox - this is an enumerated value
 *       msg_buffer - a pointer to the location in memory to be used as a buffer
 * char* get_msg_buffer(int exchange);
 *    purpose: returns a pointer to the memory buffer used for a mailbox
 *    parameters:
 *       exchange - the index of the mailbox - an enumerated value
 * void set_msg_flag(int exchange);
 *    purpose: sets the message flag for a specified mailbox
 *    parameters:
 *       exchange - the index of the mailbox - an enumerated value
 * void clr_msg_flag(int exchange);
 *    purpose: clears the message flag for a specified mailbox
 *    parameters:
 *       exchange - the index of the mailbox - an enumerated value
 * int get_msg_flag(int exchange);
 *    purpose: returns the message flag for a specified mailbox
 *    parameters:
 *       exchange - the index of the mailbox - an enumerated value 
 * int* msg_flg_ptr(int exchange); // returns pointer to the message flag
 *    purpose: returns a pointer to the message flag for a specified mailbox
 *    parameters:
 *       exchange - the index of the mailbox - an enumerated value
 * 
 * interprocess flags are general purpose flags to be used for signaling between
 * processes
 * void set_ip_flag(flags n);
 *    purpose: sets an interprocess flag specified by parameter, n
 *    parameters: n is the index of the flag to use - it is an enumerated value
 * void clr_ip_flag(flags n);
 *    purpose: clears an interprocess flag specified by parameter, n
 *    parameters: n is the index of the flag to use - it is an enumerated value
 * int get_ip_flag(flags n);
 *    purpose: returns an interprocess flag specified by parameter, n
 *    parameters: n is the index of the flag to use - it is an enumerated value
 * void init_ip_flags(void);
 *    purpose: initializes the interprocess flags by clearing all of them
 */

#ifndef MSG_PROCESSOR_H
#define	MSG_PROCESSOR_H

#include <stdio.h>
#include "sys_config.h"
#include "utilities.h"
#include "serial_terminal.h"
#include "msg_processor.h"
#include "command_processors.h"
#include "i2c_driver.h"

#ifdef BUILD_TEMPERATURE
#include "thermo.h"
#endif //BUILD_TEMPERATURE

#ifdef BUILD_GPS
#include "gps.h"
#endif //BUILD_GPS

char* parse_frm(char *src, char* vect[], char sf, char ef, char dlm, int trim);
char* build_frm(char* results, char* vect[], int cnt, char sf, char ef, char dlm);
char calc_cs(char* ptr, char sf, char ef);
int validate_cs(char MSB, char LSB, char src_cs);
void extract_cs(char* msg, char ef, char* MSB, char* LSB);
int process_msg(char* fields[], exchanges mailslot);
char** get_token_ptrs(void);
void send_command(char* fields[], int num_tokens, char* s_dest_mod);
void add_token_ptr(int position, char* pointer);

char* assign_msg_buffer(int exchange, char* msg_buffer);
char* get_msg_buffer(int exchange);
void set_msg_flag(int exchange);
void clr_msg_flag(int exchange);
int get_msg_flag(int exchange);
int* msg_flg_ptr(int exchange); // returns pointer to the message flag
void set_ip_flag(flags n);
void clr_ip_flag(flags n);
int get_ip_flag(flags n);
void init_ip_flags(void);
void clear_token_ptrs(void);
char* replace_token(char* frame, char* vect[], int n, char* new_token, char sf, char ef, char dlm);
char* get_token(char* frame, char* vect[], int n, char* result, char sf, char ef, char dlm);
int token_count(char* src, char sf, char ef, char dlm);
void send_ack(exchanges mailbox, int destination);

#endif	/* MSG_PROCESSOR_H */


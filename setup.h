/* 
 * File:   processor.h
 * Author: WHalphen
 *
 * Created on January 23, 2016, 12:00 PM
 * 
 * PURPOSE
 * Sets configuration and initializes processor
 * 
 */
/*
 * FUNCTION
 * void proc_init(void);
 *  execute this function to initialize the processor
 * FUNCTION
 * int config_init(void);
 *  execute this function to initialize the configuration
 * FUNCTION
 * void start_screen(void);
 *  execute this function to display the start screen
 * FUNCTION
 * void set_msg_buffers(void);
 *  execute this function to initialize the message buffers
 * FUNCTION
 * void initialize_i2c(void);
 *  execute this function to initialize the i2c peripheral
 *
 */

#ifndef PROCESSOR_H
#define	PROCESSOR_H

#include "sys_config.h"
#include <stdio.h>
#include "msg_processor.h"
#include "eeprom.h"

void proc_init(void);
int config_init(void);
void start_screen(void);
void set_msg_buffers(void);
void initialize_i2c(void);
int config_load(void);

#endif	/* PROCESSOR_H */


#include "thermo.h"
#ifdef BUILD_TEMPERATURE
 
int mcp9802_exists(char addr) {
    if(mcp9802_readhyst(addr) == 0x4b00) return 1;
    else return 0;
}

#ifdef TEMPERATURE_MODULE_I2C1
void mpc9802_resolution(char addr, char resolution){
    write2I2C1(addr << 1, CONFIG, resolution); // set resolution
}

int mcp9802_readtemp(char addr) {
    char temp_LSB;
    char temp_MSB;   
    write1I2C1(addr << 1, AMBIENT); // request register 0: AMBIENT
    read2I2C1(addr << 1, &temp_MSB, &temp_LSB); // read the value from the sensor
    // convert to tenths of degree C and return result
    return (int)((((long)((int)(temp_MSB) << 8) | ((int) temp_LSB & 0xff)) * 10) >> 8);
}

int mcp9802_readhyst(char addr) {
    char temp_LSB;
    char temp_MSB;   
    write1I2C1(addr << 1, HYSTERESIS); // request register 2: HYSTERESIS
    read2I2C1(addr << 1, &temp_MSB, &temp_LSB); // read the value from the sensor 
    return ((int) (temp_MSB << 8) | (int) (temp_LSB & 0xff)); 
}

#endif // TEMPERATURE_MODULE_I2C1
    
#ifdef TEMPERATURE_MODULE_I2C2
void mpc9802_resolution(char addr, char resolution){
    write2I2C2(addr << 1, CONFIG, resolution); // set resolution
}
int mcp9802_readtemp(char addr) {
    char temp_LSB;
    char temp_MSB;
    long temp1;
    int temp2;
    int temp3;
    int ret;
    write1I2C2(addr << 1, AMBIENT); // request register 0: AMBIENT
    read2I2C2(addr << 1, &temp_MSB, &temp_LSB); // read the value from the sensor
    // convert to tenths of degree C and return result
    temp2 = (((int)(temp_MSB) << 8) | ((int) temp_LSB & 0xff));
    temp3 = temp2 /256;
    temp1 = ((long)temp2 * 10) / 256;
    ret = (int)((((long)((int)(temp_MSB) << 8) | ((int) temp_LSB & 0xff)) * 10) >> 8);
    return temp1;
}

int mcp9802_readhyst(char addr) {
    char LSB;
    char MSB;   
    write1I2C2(addr << 1, HYSTERESIS); // request register 2: HYSTERESIS
    read2I2C2(addr << 1, &MSB, &LSB); // read the value from the sensor
    return (int)(MSB << 8) | (int) LSB;
}

#endif // TEMPERATURE_MODULE_I2C2
  
#endif //BUILD_TEMPERATURE


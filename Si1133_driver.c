
#include "Si1133_driver.h"
#ifdef BUILD_LIGHT
/*
 * Functions for interacting with Si1133 light sensor.
 * 
 * void si1133_reset_cmd_ctr(void);
 *    resets the command counter
 * 
 * char si1133_response0(void);
 *    checks for operation completion and for errors
 * 
 * char si1133_param_get(char param_num);
 *    reads parameter xxxxxx and returns result
 * 
 * void si1133_param_set(char param, char value);
 *    writes 'value' to parameter 'param_num'
 * 
 */

unsigned char channel_params[SI1133_PARAM_CNT][SI1133_CHANNEL_CNT]; // params shadow array
long results[SI1133_CHANNEL_CNT];
unsigned char chan_list;

unsigned long si1133_results(int channel) {
    return results[channel];
}

void si1133_reset_cmd_ctr(void) {
    write2I2C2(LIGHT_SENSOR, SI1133_CMD_REG, SI1133_RESET_CMD_CTR);
}

int si1133_reset(void) { // returns hardware ID, REV_ID
    write2I2C2(LIGHT_SENSOR, SI1133_CMD_REG, SI1133_RESET_SW);
    tb_delay_ms(25);
    int i = 1000;
    while (((si1133_response0() & 0x0f) != 0xf) && (i--));
    return ((int) si1133_read_register(SI1133_PART_ID) << 8) | si1133_read_register(SI1133_REV_ID);
}

void si1133_sensor_config(int channel, char adcconfig, char adcsens, char adcpost, char measconfig){
    channel_params[ADCCONFIG][channel] = adcconfig;
    channel_params[ADCSENS][channel] = adcsens;
    channel_params[ADCPOST][channel] = adcpost;
    channel_params[MEASCONFIG][channel] = measconfig;
}

int si1133_param_set(char param, char value) {
    char cmmnd_ctr;
    char err;
    int i = 1000;
    si1133_reset_cmd_ctr();
    cmmnd_ctr = si1133_response0() & 0xf;
    do {
        write2I2C2(LIGHT_SENSOR, SI1133_INPUT0, value);
        write2I2C2(LIGHT_SENSOR, SI1133_CMD_REG, param | 0x80);
        err = (si1133_response0() & 0x10);
        if (err) si1133_reset_cmd_ctr(); // if error, reset
    } while (err && i--); // if error, do over
    if (!i) err = 1;
    else {
        i = 1000;
        while (((si1133_response0() & 0x0f) != cmmnd_ctr + 1) && i--) { // wait for done 
            if (!i) err = 1;
        }
    }
    return err;
}

unsigned char si1133_param_get(char param) {
    char cmmnd_ctr;
    char err;
    char result;
    si1133_reset_cmd_ctr();
    cmmnd_ctr = si1133_response0() & 0xf;
    do {
        write2I2C2(LIGHT_SENSOR, SI1133_CMD_REG, param | 0x40); // get param
        err = (si1133_response0() & 0x10);
        if (err) si1133_reset_cmd_ctr(); // if error, reset
    } while (err); // if error, do over
    while ((si1133_response0() & 0x0f) != cmmnd_ctr + 1); // wait for done
    write1I2C2(LIGHT_SENSOR, SI1133_RESPONSE1_REG);
    read1I2C2(LIGHT_SENSOR, &result);
    return (unsigned char) result;
}

void si1133_channel_select(int channel, int on) { // 0 off, otherwise on
    if(on) chan_list |= (0x01 << channel);
    else chan_list &= ~(0x01 << channel);
}

void si1133_setup_channels(void) { // expects channel_params, chan_list populated
    int k = 3;
    int j;
    int i;
    int err = 0;
    unsigned char select = 0x01;
    while (k--) {
        for (i = 0; i < SI1133_CHANNEL_CNT; i++) {
            if (chan_list & select) { // if indicated in the channel list
                for (j = 0; j < SI1133_PARAM_CNT; j++) { // update the params
                    err = si1133_param_set((i << 2) + 2 + j, channel_params[j][i]);
                }
            }
            select = select << 1;
        }
        si1133_param_set(SI1133_CHAN_LIST, chan_list);
        if (err) {
            err = 0;
            si1133_reset();
            printf("Si1133 not responding -- reset.\n\r");
        }            
        else k = 0;
    }
}

#ifdef DEBUG
void dump_params(void) {
    char i;
    unsigned char value;
    for (i=0;i<0x2c;i++){
        value = si1133_param_get(i);
        printf("param %x, value %x\n\r", i, value );
    }
}

void dump_registers(void){
    unsigned char i;
    unsigned char value;
    for (i=0;i<0x2c;i++){
        value = si1133_read_register(i);
        printf("reg %x, value %x\n\r", i, value );
    }
}
#endif // DEBUG

unsigned char si1133_read_register(char register_address) {
    char result = 0;
    write1I2C2(LIGHT_SENSOR, register_address);  
    read1I2C2(LIGHT_SENSOR, &result);
    return (unsigned char) result;
}

void si1133_write_register(char register_address, char value) {
    write2I2C2(LIGHT_SENSOR, register_address, value);
}

void si1133_force(void) { // expects parameters and chan_list have been setup in advance
    unsigned char cmmnd_ctr;
    unsigned long measure;
    int i;
    char result_ptr;
    unsigned char select = 0x01;
    si1133_reset_cmd_ctr();
    write2I2C2(LIGHT_SENSOR, SI1133_IRQ_STATUS_REG, 0); // clear interrupt flags
    cmmnd_ctr = si1133_response0() & 0xf;
    write2I2C2(LIGHT_SENSOR, SI1133_CMD_REG, SI1133_FORCE); // send force command
//    while ((si1133_response0() & 0x0f) != cmmnd_ctr + 1); // wait for done
    while (!((si1133_read_register(SI1133_IRQ_STATUS_REG) & 0x3f) != (chan_list & 0x3f)));
    result_ptr = 0x13;
    for (i = 0; i < SI1133_CHANNEL_CNT; i++) { // for each channel
        if (chan_list & select) { // if indicated in the channel list
            measure = 0;
            // read bytes to results array, 16 or 24 bits, depending on ADCPOST
            measure = measure | ((unsigned long) si1133_read_register(result_ptr) << 24);
            result_ptr++;
            measure = measure | ((unsigned long) si1133_read_register(result_ptr) << 16);
            result_ptr++;
            if ((channel_params[ADCPOST][i] & 0x40) == SI1133_24BIT) { // 24 bit result
                measure = (measure) | ((unsigned long) si1133_read_register(result_ptr) << 8);
                result_ptr++;
                results[i] = ((long) measure) >> 8;
            }
                else {
                results[i] = ((long) measure) >> 16;
            }
        }
    }
}

unsigned char si1133_response0(void) {
    char result = 0;
    write1I2C2(LIGHT_SENSOR, SI1133_RESPONSE0_REG);  
    read1I2C2(LIGHT_SENSOR, &result);
    return (unsigned char) result;
}

unsigned char si1133_response1(void) {
    char result = 0;
    write1I2C2(LIGHT_SENSOR, SI1133_RESPONSE1_REG);  
    read1I2C2(LIGHT_SENSOR, &result);
    return (unsigned char) result;
}

void si1133_reset_sw(void) {
    write1I2C2(LIGHT_SENSOR, SI1133_RESET_SW);
}
#endif // BUILD_LIGHT




#include "pressure.h"
#ifdef BUILD_PRESSURE

static unsigned char pressure_crc; // crc for pressure sensor prom
static unsigned int pressure_calib[8]; // calibration coefficients for pressure sensor

#ifdef PRESSURE_MODULE_I2C2
void pressure_reset(void) {
    write1I2C2(ADDR_W, CMD_RESET); // send reset sequence
    tb_delay_ms(3); // wait for the reset sequence timing
}

unsigned long pressure_adc(char cmd) { // perform A to D conversion
    unsigned char data[3] = {0,0,0};
    
    write1I2C2(ADDR_W, CMD_ADC_CONV + cmd); // send conversion command
    switch (cmd & 0x0f) // wait necessary conversion time
    {
        case CMD_ADC_256: tb_delay_ms(1);
            break;
        case CMD_ADC_512: tb_delay_ms(3);
            break;
        case CMD_ADC_1024: tb_delay_ms(4);
            break;
        case CMD_ADC_2048: tb_delay_ms(6);
            break;
        case CMD_ADC_4096: tb_delay_ms(10);
            break;
    }
    write1I2C2(ADDR_W, CMD_ADC_READ);
    readNI2C2(ADDR_R, (char*) &data, 3);
    return ((65536 * (unsigned long) data[0]) + (256 * (unsigned long) data[1]) + (unsigned long) data[2]);
}

unsigned int pressure_prom(char coef_num) { // read calibration coefficients
    unsigned int rC[2] = {0, 0};
    write1I2C2(ADDR_W, CMD_PROM_RD + coef_num * 2); // send PROM READ command
    read2I2C2(ADDR_R, (char*) &rC[0], (char*) &rC[1]);
    return (256 * (unsigned int) rC[0]) + (unsigned int) rC[1];
}
#endif // PRESSURE_MODULE_I2C2

#ifdef PRESSURE_MODULE_I2C1
void pressure_reset(void) {
    write1I2C1(ADDR_W, CMD_RESET); // send reset sequence
    tb_delay_ms(3); // wait for the reset sequence timing
}

unsigned long pressure_adc(char cmd) { // perform A to D conversion
    unsigned char data[3] = {0,0,0};
    
    write1I2C1(ADDR_W, CMD_ADC_CONV + cmd); // send conversion command
    switch (cmd & 0x0f) // wait necessary conversion time
    {
        case CMD_ADC_256: tb_delay_ms(1);
            break;
        case CMD_ADC_512: tb_delay_ms(3);
            break;
        case CMD_ADC_1024: tb_delay_ms(4);
            break;
        case CMD_ADC_2048: tb_delay_ms(6);
            break;
        case CMD_ADC_4096: tb_delay_ms(10);
            break;
    }
    write1I2C1(ADDR_W, CMD_ADC_READ);
    readNI2C1(ADDR_R, (char*) &data, 3);
    return ((65536 * (unsigned long) data[0]) + (256 * (unsigned long) data[1]) + (unsigned long) data[2]);
}

unsigned int pressure_prom(char coef_num) { // read calibration coefficients
    unsigned int rC[2] = {0, 0};
    write1I2C1(ADDR_W, CMD_PROM_RD + coef_num * 2); // send PROM READ command
    read2I2C1(ADDR_R, (char*) &rC[0], (char*) &rC[1]);
    return (256 * (unsigned int) rC[0]) + (unsigned int) rC[1];
}
#endif // PRESSURE_MODULE_I2C1

void pressure_crc4(void) { // calculate the CRC code
    int cnt; // simple counter
    unsigned int n_rem; // crc reminder
    unsigned int crc_read; // original value of the crc
    unsigned char n_bit;
    n_rem = 0x00;
    crc_read = pressure_calib[7]; //save read CRC
    pressure_calib[7] = (0xFF00 & (pressure_calib[7])); //CRC byte is replaced by 0
    for (cnt = 0; cnt < 16; cnt++) // operation is performed on bytes
    {// choose LSB or MSB
        if (cnt % 2 == 1) n_rem ^= (unsigned short) ((pressure_calib[cnt >> 1]) & 0x00FF);
        else n_rem ^= (unsigned short) (pressure_calib[cnt >> 1] >> 8);
        for (n_bit = 8; n_bit > 0; n_bit--) {
            if (n_rem & (0x8000)) {
                n_rem = (n_rem << 1) ^ 0x3000;
            } else {
                n_rem = (n_rem << 1);
            }
        }
    }
    n_rem = (0x000F & (n_rem >> 12)); // final 4-bit remainder is CRC code
    pressure_calib[7] = crc_read; // restore the crc_read to its original place
    pressure_crc = (n_rem ^ 0x0);
}

unsigned int get_press_calib(int coeff) {
    return pressure_calib[coeff];
}

unsigned char get_press_crc(void) {
    return pressure_crc;
}

void pressure_init(void) {
    int i;
    pressure_reset();
    for (i = 0; i < 8; i++) pressure_calib[i] = pressure_prom(i);
    pressure_crc4();
}

void pressure_values(long* T, long* P) {
    unsigned long D1 = 0; // ADC value of the pressure conversion
    long D2 = 0; // ADC value of the temperature conversion
    long long dT; // difference between actual and measured temperature
    long long offset; // offset at actual temperature
    long long sens; // sensitivity at actual temperature
    D2 = pressure_adc(CMD_ADC_D2 + CMD_ADC_4096); // read D2
    D1 = pressure_adc(CMD_ADC_D1 + CMD_ADC_4096); // read D1
    // calculate 1st order pressure and temperature (MS5607 1st order algorithm)
    dT = D2 - ((long) get_press_calib(5) << 8);
    offset = (((long long) get_press_calib(2) << 17) + ((dT * get_press_calib(4)) >> 6));
    sens = ((long long) get_press_calib(1) << 16) + ((dT * get_press_calib(3)) >> 7);
    *T = (2000 + ((dT * get_press_calib(6)) >> 23));
    *P = ((((D1 * sens) >> 21) - offset) >> 15);
}

#endif // BUILD_PRESSURE





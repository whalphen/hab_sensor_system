
#include "eeprom.h"

#ifdef BUILD_EEPROM
#ifdef EEPROM_MODULE_I2C1
char addr_seq(unsigned long eeprom_addr) { // send address sequence to eeprom
    char ctl = EEPROM_BASE_ADDR;
    ctl |= 0x04; // A18 is set in hardware to 1
    if (eeprom_addr & A17_MASK) ctl |= 0x02;
    if (eeprom_addr & A16_MASK) ctl |= 0x08;   
    startI2C1();
    putI2C1(ctl & WADR); // send control byte 0xA | A16 | A18 | A17 | R/~W
    putI2C1((char) (eeprom_addr >> 8)); // send address high
    putI2C1((char) eeprom_addr); // send address low
    return ctl;
}

int eeprom_writebyte(unsigned long eeprom_addr, char eeprom_data) {
    char ctl;
    ctl = addr_seq(eeprom_addr); // send address sequence
    putI2C1(eeprom_data); // send data byte
    stopI2C1(); // initiate the write cycle
    startI2C1(); // poll for write completion
    while (putI2C1(ctl & WADR)); 
    return 0;
}

int eeprom_writepage(unsigned long eeprom_addr, char* eeprom_data) {
    char ctl;
    int cnt = 0;
    unsigned int i;
    int done = 0;
    char result = 1;
    ctl = addr_seq(eeprom_addr); // send address sequence
    i = LOG_PAGE_SIZE;
    while ((i--) && !done) { // copy string to eeprom, max LOG_PAGE_SIZE chars
        putI2C1(*eeprom_data);
        cnt++;
        if (*eeprom_data == '\0') done = 1;
        eeprom_data++;
    }
    stopI2C1(); // initiate the write cycle
    while (result == 1) { // poll for write completion
        startI2C1();
        result = putI2C1(ctl & WADR);
    }
    return cnt;
}


char eeprom_readbyte(unsigned long eeprom_addr) {
    char ctl;
    char read_data;
    ctl = addr_seq(eeprom_addr); // send address sequence
    startI2C1();
    putI2C1(ctl | RADR);
    read_data = getI2C1(I2C_NAK); // end of read
    stopI2C1();
    return read_data;
}

int eeprom_seq_read(unsigned long eeprom_addr, char* bufr, int cnt) {
    char ctl;
    int k = 128;
    int ack_nak;
    ctl = addr_seq(eeprom_addr); // send address sequence
    startI2C1();
    putI2C1(ctl | RADR);
    if (cnt) { // if cnt is not 0, then read cnt bytes
        while (cnt) {
            if (--cnt) ack_nak = I2C_ACK; // not end of read
            else ack_nak = I2C_NAK; // end of read
            *bufr = getI2C1(ack_nak);
            bufr++;
        }
        }
    else { // if cnt is 0, then read string
        while((!cnt) && k--){
            *bufr = getSI2C1();
            if(!*bufr) cnt = 1;
            bufr++;
        }
    }
    stopI2C1();
    return 0; 
}
#endif // EEPROM_MODULE_I2C1

#ifdef EEPROM_MODULE_I2C2
char addr_seq(unsigned long eeprom_addr) { // send address sequence to eeprom
    char ctl = EEPROM_BASE_ADDR;
    ctl |= 0x04; // A18 is set in hardware to 1
    if (eeprom_addr & A17_MASK) ctl |= 0x02;
    if (eeprom_addr & A16_MASK) ctl |= 0x08;   
    startI2C2();
    putI2C2(ctl & WADR); // send control byte 0xA | A16 | A18 | A17 | R/~W
    putI2C2((char) (eeprom_addr >> 8)); // send address high
    putI2C2((char) eeprom_addr); // send address low
    return ctl;
}

int eeprom_writebyte(unsigned long eeprom_addr, char eeprom_data) {
    char ctl;
    ctl = addr_seq(eeprom_addr); // send address sequence
    putI2C2(eeprom_data); // send data byte
    stopI2C2(); // initiate the write cycle
    startI2C2(); // poll for write completion
    while (putI2C2(ctl & WADR)); 
    return 0;
}

int eeprom_writepage(unsigned long eeprom_addr, char* eeprom_data) {
    char ctl;
    int cnt = 0;
    unsigned int i;
    int done = 0;
    char result = 1;
    ctl = addr_seq(eeprom_addr); // send address sequence
    i = LOG_PAGE_SIZE;
    while ((i--) && !done) { // copy string to eeprom, max LOG_PAGE_SIZE chars
        //if (i) { // write data until end of string or end of buffer)
            putI2C2(*eeprom_data);
            cnt++;
       // } else putI2C2('\0'); // if last position, put string terminator
        if (*eeprom_data == '\0') done = 1;
        eeprom_data++;
    }
    stopI2C2(); // initiate the write cycle
    while (result == 1) { // poll for write completion
        startI2C2();
        result = putI2C2(ctl & WADR);
    }
    return cnt;
}

char eeprom_readbyte(unsigned long eeprom_addr) {
    char ctl;
    char read_data;
    ctl = addr_seq(eeprom_addr); // send address sequence
    startI2C2();
    putI2C2(ctl | RADR);
    read_data = getI2C2(I2C_NAK); // end of read
    stopI2C2();
    return read_data;
}

int eeprom_seq_read(unsigned long eeprom_addr, char* bufr, int cnt) {
    char ctl;
    int ack_nak;
    int done = 0;
    ctl = addr_seq(eeprom_addr); // send address sequence
    startI2C2();
    putI2C2(ctl | RADR);
    if (cnt) { // if cnt is not 0, then read cnt bytes
        while (cnt) {
            if (--cnt) ack_nak = I2C_ACK; // not end of read
            else ack_nak = I2C_NAK; // end of read
            *bufr = getI2C2(ack_nak);
            bufr++;
        }
        }
    else { // if cnt is 0, then read string
        cnt = LOG_PAGE_SIZE;
        while((!done) && (cnt--)) {
            if(!cnt) *bufr = 0; // don't overrun buffer if EOS is missing
            else *bufr = getSI2C2();
            if(!*bufr) done = 1;
            bufr++;
        }
    }
    stopI2C2();
    return 0; 
}
#endif // EEPROM_MODULE_I2C2

// data log functions

static const char empty_pg[] = LOG_END;
int log_position; // indicates page number of next entry position in log

int get_log_start(void) {
    return LOG_START;
}

int get_log_len(void){
    return (EEPROM_PAGES - LOG_START);
}

int log_init(char* log_bufr) { // search for end marker, insert if not found
    int end_found = 0;
    int len;
    int err = 0;
    int page = LOG_START;
    len = str_len((char*) empty_pg);
    while ((!end_found) && (page < EEPROM_PAGES)) {
        eeprom_seq_read(((unsigned long) page << 7) + EEPROM_START_ADDR, log_bufr, len);
        if (!str_match((char*) empty_pg, log_bufr, len, 0)) page++;
        else {
            end_found = 1;
            log_position = page;
        }
    }
    if (!end_found) err = log_write((char*) empty_pg, LOG_START + 1);
    return err_set(DATA_LOG, err);
}

int log_write(char* bufr, int page) { //returns result code
    char read_bufr[LOG_PAGE_SIZE];
    int cnt;
    int err = 0;
//    if (str_len(bufr) > LOG_PAGE_SIZE) bufr[LOG_PAGE_SIZE-1] = 0; // prevent overrun
    cnt = eeprom_writepage(((unsigned long) page << 7) + EEPROM_START_ADDR, bufr);
    eeprom_seq_read(((unsigned long) page << 7) + EEPROM_START_ADDR, read_bufr, cnt);
    if (!str_match(bufr, read_bufr, cnt, 0)) {
        err = 11;
        printf("log_write err 11\n\r");
    }
    return err_set(DATA_LOG, err);
}

char* log_read(char* bufr, int page, int count) { //returns pointer to the bufr
    // address is page number times 128
    eeprom_seq_read(((unsigned long) page << 7) + EEPROM_START_ADDR, bufr, count);
    return bufr;
}
int log_erase(int begin) { //erase content from 'begin', return result code
    log_position = begin;
    return err_set(DATA_LOG, log_write((char*) empty_pg, begin));
}

int log_next_page(int incr) { //returns next page number - increments next page number if 'incr' is 1
    int current = log_position;
    if (incr) log_position++;
    return current;
}

char* log_status(char* bufr) { //returns a string pointer indicating: Size, used
    sprintf(bufr, "Size: %d, Used: %d\n\r", (EEPROM_PAGES - LOG_START - 1), log_next_page(0));
    return bufr;
}

int log_size(void) {
    return EEPROM_PAGES - LOG_START - 1; 
}

int log_used(void) {
    return log_next_page(0);
}

#endif // BUILD_EEPROM

int __attribute__ ((space(eedata))) nvm_data; // Global variable located in EEPROM

void nvm_write_string(int adr, char* s) {
    int data;
    int len;
    int i;
    char* ptr;
    ptr = s;
    len = str_len(ptr);   
    if (!(len % 2)) len += 1; // force length of string, with EOS, to be even
    for (i = 0; i < ((len + 1) / 2); i++) {
        data = (*ptr << 8); // combine pairs of bytes to integers
        ptr++;
        data |= *ptr;
        ptr++;
        nvm_write((2*i) + adr, data); // write to nvm
    }
}

int nvm_read_string(int adr, char* buffer) {
    char* ptr;
    int data;
    int ctr = 0; // character counter
    int eos = 0; // end of string
    ptr = buffer;
    while ((!eos) && (ctr < NVM_PAGE_SIZE)) {
        data = nvm_read(adr);
        adr += 2;
        *ptr = (char) (data >> 8);
        if (*ptr) { // not at end yet
            ctr++; // count the character         
            if (ctr < NVM_PAGE_SIZE) { // prevent buffer overrun
                 ptr++; // point to the next byte
                *ptr = (char) data;
                if (*ptr == 0) eos = 1;
                else ctr++; // count the character
            }
        } else { // at end of string or end of page, terminate string and function
            eos = 1;
            *ptr = 0;
        }
        if (!eos) ptr++;
    }
    return ctr;
}

void nvm_write(int addr, int data) {
    unsigned int offset;
    NVMCON = 0x4004;
    // Set up a pointer to the EEPROM location to be erased
    TBLPAG = __builtin_tblpage(&nvm_data); // Initialize EE Data page pointer
    offset = __builtin_tbloffset(&nvm_data) + (addr & 0x01ff); // Initialize lower word of address
    __builtin_tblwtl(offset, data); // Write EEPROM data to write latch
    asm volatile ("disi #5"); // Disable Interrupts For 5 Instructions
    __builtin_write_NVM(); // Issue Unlock Sequence & Start Write Cycle
    while (NVMCONbits.WR == 1); // Poll WR bit to wait for
    // write sequence to complete
}

int nvm_read (int addr){
    int data; // Data read from EEPROM
    unsigned int offset;

    // Set up a pointer to the EEPROM location to be erased
    TBLPAG = __builtin_tblpage(&nvm_data); // Initialize EE Data page pointer
    offset = __builtin_tbloffset(&nvm_data) + (addr & 0x01ff); // Initialize lower word of address
    data = __builtin_tblrdl(offset); // Write EEPROM data to write latch
    return data;
}





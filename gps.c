
#include "gps.h"
#ifdef BUILD_GPS

//volatile msg_proc_state msg_status; // status of message processing: 
//volatile char* msg_ptr; // pointer to the next storage location in store
//char* msg_home; // start of store
//char checksum;
//char start_flag; // character used for the start flag
//char end_flag; // character used for the end flag
//int* ready_flag; // pointer to message ready flag


// turn off unnecessary default gps messages
const char gll_off[] = "$PUBX,40,GLL,0,0,0,0*5C";
const char gsv_off[] = "$PUBX,40,GSV,0,0,0,0*59";
const char gsa_off[] = "$PUBX,40,GSA,0,0,0,0*4E";
const char rmc_off[] = "$PUBX,40,RMC,0,0,0,0*47";
const char gga_off[] = "$PUBX,40,GGA,0,0,0,0*5A";
const char vtg_off[] = "$PUBX,40,VTG,0,0,0,0*5E";
// poll for PIBX00 message
const char PUBX00[] = "$PUBX,00*33";

const char setnav[] = {  // set navigation model
    0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 0x06, 0x03,
    0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00, 0x05, 0x00,
    0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x16, 0xDC, 0x00
};

// structure to hold field values
struct gps_values {
    char time[11];
    char latitude[10];
    char ns[2];
    char longitude[13];
    char ew[2];
    char altitude[8];
    char navstat[3];
    char speed[8];
    char deg[7];
    char vvel[8];
    char sats[3];
};

static struct gps_values gps;
char* fields[MAX_FIELDS]; // array of string pointers to fields

struct gps_buffer {
    char* home;
    char* ptr;
    int status;
    char checksum;
    char start_flag;
    char end_flag;
    int* ready_flag;
};

static struct gps_buffer msg_store;

#ifdef GPS_UART1

//Initiation function. Parameter baudratereg1 determines baud speed
void gps_uart_init(int baudratereg1) {
    U1BRG = baudratereg1; //set baud speed
    U1MODE = 0x8000; //turn on module
    U1STA = 0x0400; //set interrupts condition
    IPC2bits.U1RXIP = 6; //priority 5
    IFS0bits.U1RXIF = 0; //reset RX interrupt flag
    U1STAbits.UTXEN = 1;
}

//UART transmit function, parameter ch is the character to send
void gps_putchar(char ch) {
    while (U1STAbits.UTXBF); //transmit ONLY if TX buffer is empty
    U1TXREG = ch;
}

// Receiver interrupt service routine.  Executes each time a character is
// received by the UART1 peripheral.
void __attribute__((interrupt(auto_psv))) _U1RXInterrupt(void)
{
    char c;
    c = U1RXREG;
    switch (msg_store.status) {
        case SEARCHING: // searching for start of message
            if (c == msg_store.start_flag) { // found start:
                *msg_store.ptr = c; // store value
                msg_store.ptr++; // increment output pointer
                msg_store.checksum = 0; // prep for checksum
                msg_store.status = PROCESSING; // next status
            }
            break;
        case PROCESSING: // capturing message contents
            if (c == msg_store.start_flag) { // encountered unexpected start flag
                msg_store.ptr = msg_store.home; // reset pointer to start over
                msg_store.checksum = 0; // reset checksum
            } else if (c == msg_store.end_flag) { // found end flag
                msg_store.status = CS_MSB; // next status
            } else msg_store.checksum ^= c; // checksum content of message
            *msg_store.ptr = c; // store character
            msg_store.ptr++;
            break;
        case CS_MSB:
            *msg_store.ptr = c; // store MSB character
            msg_store.ptr++;
            msg_store.status = CS_LSB; // next status
            break;
        case CS_LSB:
            *msg_store.ptr = c; // store LSB character
            msg_store.ptr++;
            *msg_store.ptr = 0; // store null to terminate string
            msg_store.ptr = msg_store.home; // reset pointer to beginning
            msg_store.status = COMPLETE; // message status complete
            *msg_store.ready_flag = 1; // message is ready to be processed
            break;
        case COMPLETE:
            IEC0bits.U1RXIE = 0; // disable RX interrupts      
    }
    IFS0bits.U1RXIF = 0; //reset interrupt flag
}

void gps_enable(void){
    msg_store.status = SEARCHING;
    U1STAbits.OERR = 0;
    IEC0bits.U1RXIE = 1;// enable RX interrupt
    gps_sendstr(PUBX00,12);
}

void gps_initialize(int baud, char* buffer, int* rdy_flg) {
    char c;
    gps_uart_init(baud);
    msg_store.ptr = buffer; // pointer to the next storage location in store
    msg_store.home = buffer; // start of store 
    c = U1RXREG;
    msg_store.status = SEARCHING;
    msg_store.checksum = 0;
    msg_store.start_flag = START_FLAG; // character used for the start flag
    msg_store.end_flag = END_FLAG; // character used for the end flag
    msg_store.ready_flag = rdy_flg;
    tb_delay_ms(500);
    gps_sendstr(gll_off, 24); //turn off undesired messages
    gps_sendstr(gsv_off, 24);
    gps_sendstr(gsa_off, 24);
    gps_sendstr(rmc_off, 24);
    gps_sendstr(gga_off, 24);
    gps_sendstr(vtg_off, 24);
    tb_delay_ms(1000);
    gps_sendstr(setnav, 45); // set navigation model
}
#endif // GPS_UART1

#ifdef GPS_UART2
//Initiation function. Parameter baudratereg determines baud speed
void gps_uart_init(int baudratereg) {
    U2BRG = baudratereg; //set baud speed
    U2MODE = 0x8000; //turn on module
    U2STA = 0x0400; //set interrupts condition
    IPC7bits.U2RXIP = 5; // interrupt priority 5
    IFS1bits.U2RXIF = 0; //reset RX interrupt flag
    U2STAbits.UTXEN = 1;
}

//UART transmit function, parameter ch is the character to send
void gps_putchar(char ch) {
    while (U2STAbits.UTXBF); //transmit ONLY if TX buffer is empty
    U2TXREG = ch;
}

// Receiver interrupt service routine.  Executes each time a character is
// received by the UART peripheral.
void __attribute__((interrupt(auto_psv))) _U2RXInterrupt(void)
{
    char c;
    c = U2RXREG;
    switch (msg_store.status) {
        case SEARCHING: // searching for start of message
            if (c == msg_store.start_flag) { // found start:
                *msg_store.ptr = c; // store value
                msg_store.ptr++; // increment output pointer
                msg_store.checksum = 0; // prep for checksum
                msg_store.status = PROCESSING; // next status
            }
            break;
        case PROCESSING: // capturing message contents
            if (c == msg_store.start_flag) { // encountered unexpected start flag
                msg_store.ptr = msg_store.home; // reset pointer to start over
                msg_store.checksum = 0; // reset checksum
            } else if (c == msg_store.end_flag) { // found end flag
                msg_store.status = CS_MSB; // next status
            } else msg_store.checksum ^= c; // checksum content of message
            *msg_store.ptr = c; // store character
            msg_store.ptr++;
            break;
        case CS_MSB:
            *msg_store.ptr = c; // store MSB character
            msg_store.ptr++;
            msg_store.status = CS_LSB; // next status
            break;
        case CS_LSB:
            *msg_store.ptr = c; // store LSB character
            msg_store.ptr++;
            *msg_store.ptr = 0; // store null to terminate string
            msg_store.ptr = msg_store.home; // reset pointer to beginning
            msg_store.status = COMPLETE; // message status complete
            *msg_store.ready_flag = 1; // message is ready to be processed
            break;
        case COMPLETE:
            IEC1bits.U2RXIE = 0; // disable RX interrupts      
    }
    IFS1bits.U2RXIF = 0; //reset interrupt flag
}


void gps_enable(void){
    msg_store.status = SEARCHING;
    U2STAbits.OERR = 0;
    IEC1bits.U2RXIE = 1;// enable RX interrupt
    mst_trigger(1);
    gps_sendstr(PUBX00,12);
}

void gps_initialize(int baud, char* buffer, int* rdy_flg) {
    char c;
    gps_uart_init(baud);
    msg_store.ptr = buffer; // pointer to the next storage location in store
    msg_store.home = buffer; // start of store 
    c = U2RXREG;
    msg_store.status = SEARCHING;
    msg_store.checksum = 0;
    msg_store.start_flag = START_FLAG; // character used for the start flag
    msg_store.end_flag = END_FLAG; // character used for the end flag
    msg_store.ready_flag = rdy_flg;
    tb_delay_ms(500);
    gps_sendstr(gll_off, 24); //turn off undesired messages
    gps_sendstr(gsv_off, 24);
    gps_sendstr(gsa_off, 24);
    gps_sendstr(rmc_off, 24);
    gps_sendstr(gga_off, 24);
    gps_sendstr(vtg_off, 24);
    tb_delay_ms(1000);
    gps_sendstr(setnav, 45); // set navigation model
}

#endif // GPS_UART2

void gps_sendstr(const char* msg, int len) {
    int i;
    for (i=0; i<(len-1); i++) gps_putchar(msg[i]);
    gps_putchar('\n');
    gps_putchar('\r');
}

int gps_get_cs(void) {
    return msg_store.checksum;
}

char* gps_get_ptr(void) {
    return msg_store.home;
}

int gps_process_msg(void) {
    char csmsb;
    char cslsb;
    int err = 0;
    extract_cs(msg_store.home, END_FLAG, &csmsb, &cslsb);
    if (validate_cs(csmsb, cslsb, msg_store.checksum)) {
        if(parse_frm(msg_store.home, fields, START_FLAG, END_FLAG, DELIMITER, 1) != msg_store.home) {
            copy_string(gps.time, fields[2]);
            copy_string(gps.latitude, fields[3]);
            copy_string(gps.ns, fields[4]);
            copy_string(gps.longitude, fields[5]);
            copy_string(gps.ew, fields[6]);
            copy_string(gps.altitude, fields[7]);
            copy_string(gps.navstat, fields[8]);
            copy_string(gps.speed, fields[11]);
            copy_string(gps.deg, fields[12]);
            copy_string(gps.vvel, fields[13]);
            copy_string(gps.sats, fields[18]);
        } else err = 2;   
    } else err = 1;
    return err;
}

char* gps_field(gps_field_names name) {
    switch (name) {
        case TIME: return gps.time;
        case LATITUDE: return gps.latitude;
        case NS: return gps.ns;
        case LONGITUDE: return gps.longitude;
        case EW: return gps.ew;
        case ALTITUDE: return gps.altitude;
        case NAVSTAT: return gps.navstat;
        case SPEED: return gps.speed;
        case DEG: return gps.deg;
        case VVEL: return gps.vvel;
        case SATS: return gps.sats;
    }
    return gps.time;
}

#endif // BUILD_GPS

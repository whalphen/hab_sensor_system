/* 
 * File:   timebase.c
 * Author: WHalphen
 *
 * Created on January 4, 2016
 */

/* 
 * This file must be built without optimization or the delay loops will be
 * killed by the optimizer.  To prevent optimization of the file, right click
 * on the file and select 'Override build options'.  Then turn off the
 * optimization.
 */

#include "timebase.h"
#include "msg_processor.h"

volatile static unsigned long tb_counter = 0; //milliseconds counter -- updated by timer interrupt
int tb_ticks; // holds value for loading the PR2 timer counter
int tb_osctun; // holds value for tuning the internal RC clock
volatile static unsigned int press_count = 0; //counts milliseconds length of switch press
volatile static int press_status = 0;
volatile static unsigned long sec_counter = 0; // seconds counter
volatile static int j = 1000;

// start timer

void tb_start(void) {
    T2CONbits.TON = 1;
}

// stop timer

void tb_stop(void) {
    T2CONbits.TON = 0;
}

struct pwm_timer {
    int on_time;
    int off_time;
    int dig_out;
    int invert;
    int state;
    int counter;
};

struct cycle_timer {
    unsigned long cycle_len; // cycle length in milliseconds
    int alarm; // 0: alarm off, 1: alarm on
    int alarm_hold; // temporary hold when cycle clock paused
    unsigned long counter; // countdown timer
};

volatile static struct pwm_timer pwm_timers[NUM_TIMERS];
volatile static struct cycle_timer cycle_timers[NUM_CYCLE_TIMERS];
volatile static int cycle_clock_status; // set to 1 to enable cycle clock

void __attribute__((__interrupt__, auto_psv)) _T2Interrupt(void) {
    int i;
    IFS0bits.T2IF = 0; //Clear the Timer2 interrupt status flag
    tb_counter++;
    if(!j) { // every 1000 ticks,        
        sec_counter++; // increment seconds counter
        j = 999; // reload countdown of 1000 minus this pass thru isr
    }
    j--;
    press_status = SWITCH_INPUT; // update switch status
    if (!SWITCH_INPUT) press_count++;
    if (cycle_clock_status) { // if the cycle clock is enabled...
        for (i = 0; i < NUM_CYCLE_TIMERS; i++) { // service the cycle timers
            if (!cycle_timers[i].counter--) { // if timed out...
                cycle_timers[i].alarm = 1; // turn on alarm flag
                cycle_timers[i].counter = cycle_timers[i].cycle_len; // reload counter
            }
        }
    }
    for (i = 0; i < NUM_TIMERS; i++) { // service the pwm timers
        if (pwm_timers[i].counter != 0) pwm_timers[i].counter--; // decrement timer
        else { // ctr is at 0, service the timer
            if (pwm_timers[i].on_time == 0) {
                pwm_timers[i].state = 0;
            } else if (pwm_timers[i].off_time == 0) { // one-shot
                pwm_timers[i].state = 1;
                pwm_timers[i].counter = pwm_timers[i].on_time;
                pwm_timers[i].on_time = 0;
            } else { // pwm
                if (pwm_timers[i].state == 0) { // toggle to ON
                    pwm_timers[i].state = 1;
                    pwm_timers[i].counter = pwm_timers[i].on_time;
                } else { // toggle to OFF
                    pwm_timers[i].state = 0;
                    pwm_timers[i].counter = pwm_timers[i].off_time;
                }
            }
        }
        switch (pwm_timers[i].dig_out) {
            case 0:
                break;
            case 1:
                DOUT1 = (pwm_timers[i].state ^ pwm_timers[i].invert) && !get_ip_flag(IPF_DIG_OUT_DISABLE);
                break;
            case 2:
                DOUT2 = (pwm_timers[i].state ^ pwm_timers[i].invert) && !get_ip_flag(IPF_DIG_OUT_DISABLE);
                break;
            case 3:
                DOUT3 = (pwm_timers[i].state ^ pwm_timers[i].invert) && !get_ip_flag(IPF_DIG_OUT_DISABLE);
                break;
            case 4:
                DOUT4 = (pwm_timers[i].state ^ pwm_timers[i].invert) && !get_ip_flag(IPF_DIG_OUT_DISABLE);
                break;
            case 5:
                DOUT5 = (pwm_timers[i].state ^ pwm_timers[i].invert) && !get_ip_flag(IPF_DIG_OUT_DISABLE);
        }
    }
}

void tb_set_ticks(int ticks) {
    tb_ticks = ticks;
}

void tb_set_osctun(int adjust) {
    tb_osctun = adjust;
}

int tb_get_ticks(void) {
    return tb_ticks;
}

int tb_get_osctun(void) {
    return tb_osctun;
}

int sw_get_press_len(void) {
    return press_count;
}

void sw_clr_press(void) {
    press_count = 0;
}

int sw_status(void) {
    return !press_status;
}

void init_pwm_timers(void) {
    int i;
    for (i = 0; i < NUM_TIMERS; i++) {
        pwm_timers[i].on_time = 0;
        pwm_timers[i].off_time = 0;
        pwm_timers[i].state = 0;
        pwm_timers[i].dig_out = 0;
        pwm_timers[i].invert = 0;
        pwm_timers[i].counter = 0;
    }
}

void init_cycle_timers(void) {
    int i;
    cycle_clock_status = 0; // turn off cycle clock
    for (i = 0; i < NUM_CYCLE_TIMERS; i++) {
        cycle_timers[i].alarm = 1;
        cycle_timers[i].cycle_len = 0;
        cycle_timers[i].alarm_hold = 0;
    }
}

void cycle_timer_set(int timer, unsigned long time, int alarm) {
    cycle_timers[timer].cycle_len = time;
    cycle_timers[timer].counter = time;
    cycle_timers[timer].alarm = alarm; // set the alarm state to parameter value
}

int cycle_timer_check(int timer) {
    int timed_out = 0;
    if (cycle_timers[timer].alarm) {
        timed_out = 1;
        cycle_timers[timer].alarm = 0;
    }
    return timed_out;
}

void cycle_clock_enable(int enable) { // use this to first turn on the clock
    // because cycle_clock_run will destroy the initial awake/ snooze status
    cycle_clock_status = enable;
}

void cycle_clock_pause(void) {
    int i;
    cycle_clock_status = 0;
    for (i=0;i<NUM_CYCLE_TIMERS;i++) {
        cycle_timers[i].alarm_hold = cycle_timers[i].alarm;
        cycle_timers[i].alarm = 0;
    }
}

void cycle_clock_run(void) {
    int i;
    for (i = 0; i < NUM_CYCLE_TIMERS; i++) {
        cycle_timers[i].alarm = cycle_timers[i].alarm_hold;
    }
    cycle_clock_status = 1;
}

// initialize and start the timebase

void tb_initialize(unsigned int count) {
    //instr clk src; no prescale
    T2CON = 0; // stop timer and reset control register
    TMR2 = 0; // zero contents of timer
    PR2 = count; // load period register
    init_pwm_timers();
    init_cycle_timers();
    IFS0bits.T2IF = 0; //Clear the Timer2 interrupt status flag
    IPC1bits.T2IP = 6; // Priority 6
    IEC0bits.T2IE = 1; //Enable Timer2 interrupts
    T2CONbits.TON = 1; // Start timer
}

// get counts since timer initialized

unsigned long tb_count() {
    return tb_counter;
}

void tb_delay_ms(unsigned int milliseconds) {
    unsigned long end;
    unsigned long ctr;
    end = tb_counter + (unsigned long) milliseconds;
    ctr = tb_counter;
    while (ctr < end) ctr = tb_counter;
}

void tb_clear_counter() {
    tb_counter = 0;
}

void pwm_timer_set(int tmr_num, int on, int off, int dig_out, int invert) {
    pwm_timers[tmr_num].on_time = on;
    pwm_timers[tmr_num].off_time = off;
    pwm_timers[tmr_num].dig_out = dig_out;
    pwm_timers[tmr_num].invert = invert;
}

void pwm_timer_reset(int n) {
    pwm_timers[n].on_time = 0;
    pwm_timers[n].off_time = 0;
    pwm_timers[n].counter = 0;
    pwm_timers[n].state = 0;
    pwm_timers[n].invert = 0;
    switch (pwm_timers[n].dig_out) {
        case 0:
            break;
        case 1:
            DOUT1 = 0;
            break;
        case 2:
            DOUT2 = 0;
            break;
        case 3:
            DOUT3 = 0;
            break;
        case 4:
            DOUT4 = 0;
            break;
        case 5:
            DOUT5 = 0;
    }
}

int pwm_timer(int n){
    return pwm_timers[n].state;
}

unsigned long tb_seconds(void) {
    return sec_counter;
}

void tb_clr_seconds(void) {
    sec_counter = 0;
}

#ifdef BUILD_PPS
int pps_ctr;
volatile static unsigned long last_time = 0; // holds value of tb_counter from last pps cycle

volatile static int tb_adjust(void) {
    unsigned long this_time;
    long period;
    int delta;
    this_time = tb_count();
    if ((last_time != 0) && (last_time < this_time)) {
        period = this_time - last_time;
        delta = period - 1000;
        if (((delta) > 5) && (delta < 250)) {
            PR2 = PR2 + CLOCK_ADJUST_INCR; // adjust period
        }
        else if ((delta < 6) && (delta > 2)) {
            if (OSCTUNbits.TUN != 0x20) OSCTUNbits.TUN = (((OSCTUNbits.TUN << 2) - 4) >> 2);
        }
        else if (((-(delta)) > 5) && (delta > -250)) {
            PR2 = PR2 - CLOCK_ADJUST_INCR; // adjust period
        }
        else if ((-delta < 6) && (-delta > 2)) {
            if (OSCTUNbits.TUN != 0x1f) OSCTUNbits.TUN = OSCTUNbits.TUN + 1;
        }
    }
    last_time = this_time;
    return delta;
}

int pps_seconds(void) {
    return pps_ctr;
}

void __attribute__((__interrupt__, auto_psv)) _INT1Interrupt(void)
 { 
    IFS1bits.INT1IF = 0; // clear interrupt status flag
    IEC1bits.INT1IE = 0; // disable the interrupt
    
//    printf("PR2: %d, Tun: %x, delta: %d\n\r", PR2, OSCTUNbits.TUN, tb_adjust());
    tb_adjust();
    pps_ctr++; // count seconds
    IEC1bits.INT1IE = 1; // enable the interrupt
}
#endif // BUILD_PPS

void pps_init(void){
#ifdef BUILD_PPS
    // configure external interrupt 1 which receives PPS signal from GPS
    INTCON2bits.INT1EP = 0; // trigger on rising edge
    IPC5bits.INT1IP = 7; // priority 7
    IEC1bits.INT1IE = 1; // enable the interrupt
    IFS1bits.INT1IF = 0; // clear interrupt status flag
    pps_ctr = 0;
#endif // BUILD_PPS
}

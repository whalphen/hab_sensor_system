/* 
 * File:   error.h
 * Author: WHalphen
 *
 * Created on April 23, 2016, 9:58 AM
 */

#ifndef ERROR_H
#define	ERROR_H

#include "sys_config.h"

// int err_clear(int module); // clear error code for specified module, 
//  returns error code before clearing
// int err_set(int module, int error_code); // sets error code for specified 
//  module if not zero, returns error code
// void err_init(void); // initializes last_err array values to 0

int err_clear(int module);
int err_set(int module, int error_code);
void err_init(void);

#endif	/* ERROR_H */


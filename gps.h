/* 
 * File:   gps.h
 * Author: WHalphen
 *
 * Created on January 17, 2016, 9:43 AM
 */

#ifndef GPS_H
#define	GPS_H

//
// To get gps readings:
// 1. Execute the gps_initialize function once. 
// 2. Execute the gps_enable function to trigger the gps to send a message.
// 3. Monitor the rdy_flg flag to determine when the message is ready.
//    Once the rdy_flg is a 1, the gps will send no more
//    messages until the gps_enable function re-triggers it.
// 4. To process the message, use the gps_process_msg function.
// 5. The field values can be retrieved using the gps_field function.
//

/* FUNCTION
 *   void gps_initialize(int baud, char* buffer, int* rdy_flg);
 *     Initializes the gps and uart1.  Turns off unnecessary gps messages. This
 *     function must be called once before using the gps.
 *   PARAMETERS: 
 *     baud -- baud rate generator counter value for uart1
 *     buffer -- pointer to the buffer to hold the messages from the gps
 *     rdy_flg -- pointer to a flag to be used to indicate message ready
 * FUNCTION
 *   void gps_enable(void); 
 *     Turns on the interrupt service routine to allow incoming messages from
 *     the gps.  This function must be executed when ready to receive a gps
 *     message.
 * FUNCTION
 *   int gps_process_msg(void);
 *     This function processes the message string.  
 *     The string is in an NMEA message frame format.  The function
 *     parses the content into separate strings for each field.  A zero value
 *     is returned if the parsing is successful.  If the parsing fails, a 2 is
 *     returned.  If the checksum is not valid, a 1 is returned.  This function
 *     must be executed once before retrieving the field values with the
 *     gps_field function.
 * FUNCTION
 *   char* gps_field(gps_field_names name);
 *     The gps_field function is used to retrieve the field values after the
 *     gps_process_msg function is executed.  The gps_field function returns a
 *     pointer to the field string.  The name parameter is used to specify
 *     the field to return.  Valid name parameter values are: TIME, LATITUDE,
 *     NS, LONGITUDE, EW, ALTITUDE, NAVSTAT, SPEED, DEG, VVEL, SATS 
 * FUNCTION
 *   char* gps_get_ptr(void);
 *     This function returns a pointer to the buffer used to receive gps
 *     messages.
 * 
 * 
 * PRIVATE FUNCTIONS
 * FUNCTION
 *   void gps_sendstr(const char* msg, int len);
 *   sends a string of characters, 'msg', to gps
 *   PARAMETERS
 *     msg - pointer to sequence to be sent
 *     len - length of sequence to be sent
 * FUNCTION
 *   void gps_uart_init(int baudratereg);
 *   Initializes uart, which is used to communicate between the PIC and GPS.
 *   PARAMETER: baudratereg is the counter value used to establish the baud rate
 * FUNCTION
 *   void gps_putchar(char ch);
 *   Sends the character specified by ch to the uart transmitter.  This is 
 *   used to send messages to the GPS.
 * FUNCTION
 *   void __attribute__((interrupt(auto_psv))) _U1RXInterrupt(void)
 *   This is the interrupt service routine for the uart1 receiver.  It executes
 *   each time a character is received from the gps while the message ready
 *   indicator is 0.
 * 
 * After the gps_enable function is called, the gps sends a message.  At most one
 * message is received each time the gps_enable function is executed.  The
 * message is received into the specified buffer.  The gps_chk_msg_rdy()
 * function returns a 1 when the message has been completely received. After
 * that the gps_process message function is executed to parse the message.  The
 * gps_field function can then be used to retrieve the individual field values
 * obtained from the message.
 * 
 * When the gps_process_msg function executes, it checks characters start from
 * the front of the message as follows:
 * It determines whether the character is a start flag.  If not, it ignores the 
 * character.  If it finds a start flag, it stores it and continues to read  
 * characters until it finds another start flag (which results in restarting
 * the scan) or an end flag.  After finding an end flag, it reads and confirms
 * a following two character checksum.  The checksum is calculated by
 * doing an exclusive-or of all the characters between the start flag and the 
 * stop flag. 
 * 
 * The status values used by the gps_process_msg function are:
 *     SEARCHING, // searching for start flag
 *     PROCESSING, // capturing message content
 *     CS_MSB, // fetching checksum MSB
 *     CS_LSB, // fetching checksum LSB
 *     COMPLETE // complete message received
 *  
 */

#include "sys_config.h"
#ifdef BUILD_GPS

#include "serial_terminal.h"
#include "timebase.h"
#include "utilities.h"
#include "msg_processor.h"

//#define BAUD_1200 832
//#define BAUD_9600 103
//#define BAUD_19200 52
//#define BAUD_115200 8

typedef enum {
    TIME,
    LATITUDE,
    NS,
    LONGITUDE,
    EW,
    ALTITUDE,
    NAVSTAT,
    SPEED,
    DEG,
    VVEL,
    SATS
} gps_field_names;

typedef enum { // message processing states
    SEARCHING, // searching for start flag
    PROCESSING, // capturing message content
    CS_MSB, // fetching checksum MSB
    CS_LSB, // fetching checksum LSB
    COMPLETE // complete message received
} msg_proc_state; 

//Public functions
void gps_initialize(int baud, char* buffer, int* rdy_flg); // initializes gps and uart
void gps_enable(void); // enables receive from gps
int gps_process_msg(void);
char* gps_get_ptr(void); // get pointer to buffer
int gps_get_cs(void); // gets calculated checksum
char* gps_field(gps_field_names name);

//Private functions
void __attribute__((interrupt(auto_psv))) _U1RXInterrupt(void); // isr
void gps_sendstr(const char *msg, int len); // send string to gps
void gps_uart_init(int baudratereg1); // initialize uart1
void gps_putchar(char ch); // transmit character to uart1

#endif /* BUILD_GPS */
#endif	/* GPS_H */


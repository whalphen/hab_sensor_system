/* 
 * File:   hab_main.c
 * Author: WHalphen
 *
 * Created on December 17, 2015, 9:36 AM
 */

#include "hab_main.h"

struct record {
    char ticks[9]; // second counter
    char time[8]; // gps time
    char lat[12]; // latitude
    char lon[13]; // longitude
    char altitude[7]; // altitude
    char navstat[3]; // gps status
    char sog[5]; // speed
    char cog[4]; // course
    char vvel[5]; // vertical velocity
    char sats[3]; // number of satellites
    char xtemp[9]; // external temp
    char itemp[9]; // internal temp
    char press[8]; // barometric pressure
    char vbat[5]; // battery voltage
    char ir[8]; // IR light
    char white[8]; // white light
    char uv[8]; // uv light
}
;

static struct record data;
int altitude;
int previous_altitude = 0;

char* const record_ptrs[RECORD_FIELDS] = {
    data.ticks,
    data.time,
    data.lat,
    data.lon,
    data.altitude,
    data.navstat,
    data.sog,
    data.cog,
    data.vvel,
    data.sats,
    data.xtemp,
    data.itemp,
    data.press,
    data.vbat,
    data.ir,
    data.white,
    data.uv
};

void clear_data(void) {
    int i;
    for (i = 0; i < RECORD_FIELDS; i++)  *record_ptrs[i] = 0;
}

void build_record(void) {
    int i;
    char num[3];
    sprintf(num, "%d", RECORD_FIELDS + 1); // make string for number of fields
    add_token_ptr(0, "en"); // ENTRY command
    add_token_ptr(1, num); // number of fields
    add_token_ptr(2, "D"); // D for data record type
    for (i = 3; i < (RECORD_FIELDS + 3); i++) { // fill token pointers
        add_token_ptr(i, record_ptrs[i - 3]);
    }
    add_token_ptr(RECORD_FIELDS + 3, '\0'); // and one more to terminate the list
}

void load_gps_data(void) {
    sprintf(data.time, "%.6ld", atol(get_token_ptrs()[2]));
    copy_string(data.lat, get_token_ptrs()[3]);
    copy_string(data.lon, get_token_ptrs()[5]);
    sprintf(data.altitude, "%ld", (((atol(get_token_ptrs()[7]) * 10) + 5) / 10));
    copy_string(data.navstat, get_token_ptrs()[8]);
    sprintf(data.sog, "%d", (((atoi(get_token_ptrs()[11]) * 10) + 5) / 10));
    sprintf(data.cog, "%d", (((atoi(get_token_ptrs()[12]) * 10) + 5) / 10));
    sprintf(data.vvel, "%d", (((atoi(get_token_ptrs()[13]) * 10) + 5) / 10));
    copy_string(data.sats, get_token_ptrs()[18]);
}

void check_manual_erase(void) {
#if ((INCLUDE_EEPROM_MODULE==1) || (INCLUDE_CENTRAL_PROCESSOR==1))
    if ((sw_get_press_len() > 5000) && sw_status()) {
        pwm_timer_set(ERASE_LOG, 100, 100, LED2, NOINVERT);
    } else {
        pwm_timer_set(ERASE_LOG, 0, 0, LED2, NOINVERT);
        if (!sw_status()) sw_clr_press();
    }
    if ((sw_get_press_len() > 10000) && sw_status()) {
        pwm_timer_set(ERASE_LOG, 300, 0, LED2, NOINVERT);
        printf("Erasing log...");
    #ifdef BUILD_EEPROM
        log_erase(LOG_START + 1);
    #else
        get_token_ptrs()[0] = "NEW"; // command for erasing log
        send_command(get_token_ptrs(), 1, ID_LOG);
    #endif // BUILD_EEPROM
        printf("Erased.\n\r");
        pwm_timer_set(ERASE_LOG, 0, 0, LED2, NOINVERT);
        sw_clr_press();
    }
#endif 
}

int main(void) {
    
/* initialize the system ******************************************************/
proc_init(); // initialize processor
    wdt(ON); // turn on watchdog timer
    set_trace(OFF); // trace off initially
    set_debug(OFF); // debug off initially
    set_msg_buffers(); // assign message buffers to mailboxes
    pps_init(); // initialize PPS interrupt
    initialize_i2c(); // initialize the i2c peripherals
    tb_initialize(PROC_TICKS_PER_COUNT); // initialize timebase
    pwm_timer_set(HEARTBEAT, 100, 900, LED1, NOINVERT);
    init_ip_flags(); // initialize inter-process flags
    err_init(); // initialize error tracking
    __C30_UART = UI_UART; // select uart for stdio
    ser_init(BAUD_9600, msg_flg_ptr(UI), get_msg_buffer(UI)); // intialize uart
#ifdef BUILD_SLAVE_PROCESSOR
    printf("I2C Slave Address: %x", I2C1ADD);
#endif // BUILD_SLAVE_PROCESSOR
#ifdef BUILD_VOLTMETER
    adc_init();
#endif // BUILD_VOLTMETER
#ifdef BUILD_PRESSURE
    pressure_init(); // initialize pressure sensor
#endif // BUILD_PRESSURE
#ifdef BUILD_GPS  // initializes gps and uart2 
    gps_initialize(BAUD_9600, get_msg_buffer(GPS), msg_flg_ptr(GPS));
#endif // BUILD_GPS           
    tb_delay_ms(1000);
    start_screen(); // show start screen
#ifdef BUILD_TEMPERATURE
    if (mcp9802_exists(SENSOR1)) {
        mpc9802_resolution(SENSOR1, RES0625); // set temp sensor resolution
        printf("Temperature sensor detected.\n\r>");
    } else {
        pwm_timer_set(ERROR, ERR_PD, ERR_PD, LED2, NOINVERT);
        printf("Temperature sensor not detected.\n\r>");
    }
#endif // BUILD_TEMPERATURE
#ifdef BUILD_LIGHT
    char adcconfig;
    char adcsens = SI1133_RANGE_HIGH | SI1133_SW_GAIN_1 | SI1133_HW_GAIN_1;
    char adcpost = SI1133_24BIT;
    char measconfig = 0;
    wdt(1);
    if ((si1133_reset() & 0xff00) == 0x3300) {
        printf("Light sensor detected.\n\r>");
    } else {
        pwm_timer_set(ERROR, ERR_PD, ERR_PD, LED2, NOINVERT);
        printf("Light sensor not detected.\n\r>");
    }
    adcconfig = SI1133_RATE_1024 | SI1133_SMALL_IR;
    si1133_sensor_config(0, adcconfig, adcsens, adcpost, measconfig);
    adcconfig = SI1133_RATE_1024 | SI1133_MEDIUM_IR;
    si1133_sensor_config(1, adcconfig, adcsens, adcpost, measconfig);
    adcconfig = SI1133_RATE_1024 | SI1133_LARGE_IR;
    si1133_sensor_config(2, adcconfig, adcsens, adcpost, measconfig);
    adcconfig = SI1133_RATE_1024 | SI1133_WHITE;
    si1133_sensor_config(3, adcconfig, adcsens, adcpost, measconfig);
    adcconfig = SI1133_RATE_1024 | SI1133_LARGE_WHITE;
    si1133_sensor_config(4, adcconfig, adcsens, adcpost, measconfig);
    adcconfig = SI1133_RATE_512 | SI1133_UV;
    adcsens = SI1133_RANGE_NORMAL | SI1133_SW_GAIN_128 | SI1133_HW_GAIN_4;
    si1133_sensor_config(5, adcconfig, adcsens, adcpost, measconfig);
#endif // BUILD_LIGHT
#ifdef BUILD_EEPROM
    printf("Initializing data log..."); // initialize eeprom
    if(log_init(get_msg_buffer(LOG))) printf("Data Log Error: %d\n\r", err_clear(DATA_LOG)); 
    printf("initialized.\n\n\r>");
//    record_prep(); // initialize array of pointers to the data fields
#endif // BUILD_EEPROM
    config_init(); // prep non-volatile config storage
    // set timers
    config_load(); // load cycle timers from config
    cycle_clock_enable(1); // turn on cycle clock
    tb_clr_seconds(); // zero the seconds counter
    clear_token_ptrs(); // fill the token pointers with nulls

    /******************************************************************************/
    char** vector = get_token_ptrs();
    
#ifdef BUILD_CENTRAL_PROCESSOR
    int i;
    char* buffer = get_msg_buffer(MISO);

    // reset all slave modules
    char id[2];
    for (i = 0; i < 5; i++) {
        vector[0] = "RESET";
        sprintf(id, "%d", i + 1);
        send_command(vector, 1, id);
    }
    *buffer = 0; // clear MISO buffer by inserting End of String character
    tb_delay_ms(3000);
     
    // initially set gps error on until satellite lock is confirmed
    sprintf(get_msg_buffer(MOSI),"$PWM,%d,%d,%d,%d,%d,%d*", ERROR, ERR_PD, ERR_PD, LED2, NOINVERT, MODULE_ID_GPS);
    set_msg_flag(MOSI);
    process_msg(vector, MOSI);
#endif // BUILD_CENTRAL_PROCESSOR
    
    // turn on audio beacon
    sprintf(get_msg_buffer(MOSI),"$PWM,%d,%d,%d,%d,%d,%d*", ALARM_TIMER, ALM_ON, ALM_OFF, ALARM, NOINVERT, MODULE_ID_PRESS);
    set_msg_flag(MOSI);
    process_msg(vector, MOSI);


    /*==== start of main loop =====================================================*/
    while (1) {
        process_msg(vector, UI);
#ifdef BUILD_SLAVE_PROCESSOR
        process_msg(get_token_ptrs(), MOSI);
#endif // BUILD_SLAVE_PROCESSOR

#if ((INCLUDE_EEPROM_MODULE==1) || (INCLUDE_CENTRAL_PROCESSOR==1))
        check_manual_erase();
#endif

        if (cycle_timer_check(SHORT)) {
//*****************************************************************************
// Place instructions to execute on a short cycle in this section
            
//*****************************************************************************
        }

        if (cycle_timer_check(MED)) {
            //*****************************************************************************
            // Place instructions to execute on a medium cycle in this section     
#ifdef BUILD_CENTRAL_PROCESSOR

            wdt_clr(); // reset the watchdog timer

            // start gps measurement
            vector[0] = "E";
            send_command(vector, 1, ID_GPS);
            pwm_timer_set(DELAY_TIMER, 1200, 0, 0, 0); // countdown for gps results
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // get seconds counter value
            sprintf(data.ticks, "%lu", tb_seconds());

            // get pressure measurement
            vector[0] = "P";
            send_command(vector, 1, ID_PRESS);
            tb_delay_ms(PRESS_DELAY);
            readSI2C1((I2C_SLAVE_BASE_ADR + MODULE_ID_PRESS - 1) << 1, buffer);
            if (get_trace()) printf("Read: %s\n\r", buffer);
            if (buffer != parse_frm(buffer, vector, START_FLAG, END_FLAG, DELIMITER, 1)) {
                if (str_match("PRES", vector[0], 4, 0)) {
                    copy_string(data.itemp, vector[1]);
                    copy_string(data.press, vector[2]);
                    if ((*vector[2] != 0) && (*vector[1] != 0)) {
                        vector[0] = "ACK";
                        sprintf(id, "%d", MODULE_ID_PRESS);
                        send_command(vector, 1, id);
                    }
                }
            }
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // get temperature, battery measurements
            vector[0] = "T";
            send_command(vector, 1, ID_TEMP);
            tb_delay_ms(TEMP_DELAY);
            readSI2C1((I2C_SLAVE_BASE_ADR + MODULE_ID_TEMP - 1) << 1, buffer);
            if (get_trace()) printf("Read: %s\n\r", buffer);
            if (buffer != parse_frm(buffer, vector, START_FLAG, END_FLAG, DELIMITER, 1)) {
                if (str_match("TEMP", vector[0], 4, 0)) {
                    copy_string(data.xtemp, vector[1]);
                    copy_string(data.vbat, vector[2]);
                    if ((*vector[2] != 0) && (*vector[1] != 0)) {
                        vector[0] = "ACK";
                        sprintf(id, "%d", MODULE_ID_TEMP);
                        send_command(vector, 1, id);
                    }
                }
            }
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // get light measurements
            vector[0] = "LI";
            send_command(vector, 1, ID_LIGHT);
            tb_delay_ms(SI1133_DELAY);

            // at this point, allow 1.2 second timer to expire before proceeding
            // because the light sensor and the gps require a long time before
            // results are ready           
            while (pwm_timer(DELAY_TIMER));

            readSI2C1((I2C_SLAVE_BASE_ADR + MODULE_ID_LIGHT - 1) << 1, buffer);
            if (get_trace()) printf("Read: %s\n\r", buffer);
            if (buffer != parse_frm(buffer, vector, START_FLAG, END_FLAG, DELIMITER, 1)) {
                if (str_match("LITE", vector[0], 4, 0)) {
                    copy_string(data.ir, vector[1]);
                    copy_string(data.white, vector[4]);
                    copy_string(data.uv, vector[6]);
                    if ((*vector[1] != 0) && (*vector[4] != 0) && (*vector[6] != 0)) {
                        vector[0] = "ACK";
                        sprintf(id, "%d", MODULE_ID_LIGHT);
                        send_command(vector, 1, id);
                    }
                }
            }
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // get gps results
            //            while (!pwm_timer(DELAY_TIMER)); // wait for 1 sec timer to start
            vector[0] = "F";
            send_command(vector, 1, ID_GPS);
            tb_delay_ms(GPS_DELAY);
            readSI2C1((I2C_SLAVE_BASE_ADR + MODULE_ID_GPS - 1) << 1, buffer);
            if (get_trace()) printf("Read: %s\n\r", buffer);
            if (buffer != parse_frm(buffer, vector, START_FLAG, END_FLAG, DELIMITER, 1)) {
                if (str_match("PUBX", vector[0], 4, 0)) {
                    vector[0] = "ACK";
                    sprintf(id, "%d", MODULE_ID_GPS);
                    send_command(vector, 1, id);
                    tb_delay_ms(ACK_DELAY);
                    parse_frm(buffer, vector, START_FLAG, END_FLAG, DELIMITER, 1);
                    load_gps_data();
                    if ((data.navstat[0] == 'D') || (data.navstat[0] == 'G')) {
                        sprintf(get_msg_buffer(MOSI), "$PWM,%d,%d,%d,%d,%d,%d*", ERROR, 0, 0, LED2, NOINVERT, MODULE_ID_GPS);
                    } else
                        sprintf(get_msg_buffer(MOSI), "$PWM,%d,%d,%d,%d,%d,%d*", ERROR, ERR_PD, ERR_PD, LED2, NOINVERT, MODULE_ID_GPS);
                }
                set_msg_flag(MOSI);
                process_msg(vector, MOSI);
            }
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // build record and make log entry
            build_record();
            send_command(vector, 23, ID_LOG);
            tb_delay_ms(LOG_DELAY);
            readSI2C1((I2C_SLAVE_BASE_ADR + MODULE_ID_LOG - 1) << 1, buffer);
            printf("Log entry: %s\n\r", buffer);
            vector[0] = "ACK";
            sprintf(id, "%d", MODULE_ID_LOG);
            send_command(vector, 1, id);
            clear_token_ptrs(); // fill the token pointers with nulls
            *buffer = 0; // clear MISO buffer by inserting End of String character

            // turn on audio beacon
            sprintf(get_msg_buffer(MOSI), "$PWM,%d,%d,%d,%d,%d,%d*", ALARM_TIMER, ALM_ON, ALM_OFF, ALARM, NOINVERT, MODULE_ID_PRESS);
            set_msg_flag(MOSI);
            process_msg(vector, MOSI);

            // altitude conditionals
            altitude = atoi(data.altitude);
            if (altitude < ALT_THRESHOLD) { // above 1000 meters, turn off led, alarm
                for (i = 0; i < 6; i++) {
                    sprintf(get_msg_buffer(MOSI), "$FLAG,%d,%d,%d*", IPF_DIG_OUT_DISABLE, 0, i);
                    set_msg_flag(MOSI);
                    process_msg(vector, MOSI);
                }
            } else {
                if (altitude > ALT_THRESHOLD) {
                    for (i = 0; i < 6; i++) {
                        sprintf(get_msg_buffer(MOSI), "$FLAG,%d,%d,%d*", IPF_DIG_OUT_DISABLE, 1, i);
                        set_msg_flag(MOSI);
                        process_msg(vector, MOSI);
                    }
                }
            }
            clear_data();
            pwm_timer_set(ALARM_TIMER, 1000, 4000, ALARM, NOINVERT);


#endif //BUILD_CENTRAL_PROCESSOR
#if (CONFIGURATION==MODULE_ID_COMBO) // Combined module

            long T;
            long P;
            wdt_clr(); // reset the watchdog timer
            clr_msg_flag(GPS); // clear message flag as msg will be overwritten
            gps_enable(); // request gps data
            pwm_timer_set(DELAY_TIMER, 1200, 0, 0, 0); // countdown for gps results
            pressure_values(&T, &P); // get pressure sensor values
            sprintf(data.itemp, "%ld", T); // press sensor temp
            sprintf(data.press, "%ld", P); // pressure
            sprintf(data.xtemp, "%d", mcp9802_readtemp(SENSOR1)); // external temp
            sprintf(data.ticks, "%lu", tb_seconds());
            sprintf(data.vbat, "%ld", ((long) (adc_get()) * 1000) / 656); // battery

            // at this point, allow 1.2 second timer to expire before proceeding
            // because the gps requires a long time before
            // results are ready           
            while (pwm_timer(DELAY_TIMER));

            if (get_debug()) printf("\n%s\n\r", get_msg_buffer(GPS));
            clear_token_ptrs();
            if (get_msg_flag(GPS)) {
                parse_frm(get_msg_buffer(GPS), get_token_ptrs(), START_FLAG, END_FLAG, DELIMITER, 1);
                load_gps_data();
            }

            // confirm gps lock
            if ((data.navstat[0] == 'D') || (data.navstat[0] == 'G')) {
                pwm_timer_set(ERROR, 0, 0, LED2, NOINVERT);
            } else pwm_timer_set(ERROR, ERR_PD, ERR_PD, LED2, NOINVERT);

            build_record();
            if (get_debug()) printf("T: %ld, ", tb_count());

            log_entry(get_token_ptrs());

            // turn on audio beacon
            pwm_timer_set(ALARM_TIMER, 1000, 4000, ALARM, NOINVERT);

            if ((data.navstat[0] == 'D') || (data.navstat[0] == 'G')) {
                // altitude conditionals
                altitude = atoi(data.altitude);
                // above 1000 meters, turn off led, alarm
                if (altitude > ALT_THRESHOLD) {
                    set_ip_flag(IPF_DIG_OUT_DISABLE);
                } else if (altitude < ALT_THRESHOLD) {
                    clr_ip_flag(IPF_DIG_OUT_DISABLE);
                }
            }
            clear_data();
        }

#endif // (CONFIGURATION==MODULE_ID_COMBO)
        /******************************************************************************/
    }
    
            if (cycle_timer_check(LONG)) {
//*****************************************************************************
// Place instructions to execute on a long cycle in this section
            
            
//*****************************************************************************
        }
        

/*==== end of main loop =====================================================*/
        return 0;
}

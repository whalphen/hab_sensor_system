/* 
 * File:   thermo.h
 * Author: WHalphen
 *
 * Created on February 2, 2016, 12:00 AM
 * 
 * FUNCTION
 * int mcp9802_readtemp(char addr); -- returns temperature in degrees C from
 * the mcp9802 sensor at device address, addr, on the I2C bus.
 * 
 * PARAMETER: addr -- This is the I2C device address for the mcp9802.
 * 
 * FUNCTION
 * void mpc9802_resolution(char addr, char resolution);
 *  Sets the conversion resolution for the sensor.  Acceptable vales for 
 *  resolution are:
 *  #define RES0625 0x60
 *  #define RES1250 0x40
 *  #define RES2500 0x20
 *  #define RES5000 0x00
 *  Parameter addr is device address for sensor 1 SENSOR1 0x4d
 *
 */

#ifndef THERMO_H
#define	THERMO_H

#include "sys_config.h"
#ifdef BUILD_TEMPERATURE

#include "i2c_driver.h"

int mcp9802_readtemp(char addr);
void mpc9802_resolution(char addr, char resolution);
int mcp9802_readhyst(char addr);
int mcp9802_exists(char addr);

#endif //BUILD_TEMPERATURE

#endif	/* THERMO_H */

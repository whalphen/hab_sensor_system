
#include "setup.h"

// Configuration bits

// FBS
#pragma config BSS = OFF    // Boot segment Protect->No boot program flash segment
#pragma config BWRP = OFF    // Boot Segment Write Protect->Disabled

// FGS
#pragma config GSS0 = OFF    // General Segment Code Protect->No Protection
#pragma config GWRP = OFF    // General Segment Write Protect->General segment may be written

// FOSCSEL
#pragma config LPRCSEL = HP    // LPRC Oscillator Power and Accuracy->High Power, High Accuracy Mode
#pragma config IESO = ON    // Internal External Switch Over bit->Internal External Switchover mode enabled (Two-speed Start-up enabled)

#ifdef EXT_OSC
#pragma config FNOSC = PRI    // Oscillator Select->Primary Oscillator (XT, HS, EC)
#endif // EXT_OSC

#ifdef INT_OSC
#ifdef CK32MHZ
#pragma config FNOSC = FRCPLL    // Oscillator Select->Fast internal oscillator w/ PLL
#endif // CK32MHZ
#ifdef CK16MHZ
#pragma config FNOSC = FRCPLL    // Oscillator Select->Fast RC oscillator w/PLL
#endif // CK8MHZ
#ifdef CK8MHZ
#pragma config FNOSC = FRC    // Oscillator Select->Fast RC oscillator
#endif // CK8MHZ
#ifdef CK4MHZ
#pragma config FNOSC = FRCDIV    // Oscillator Select->Fast RC oscillator w/ postscaler
#endif // CK8MHZ
#endif // INT_OSC

#pragma config SOSCSRC = DIG    // SOSC Source Type->Analog Mode for use with crystal *** IF NOT SET TO DIG, WILL PREVENT USE AS DIG IO !!! ***

// FOSC
#pragma config POSCFREQ = HS    // Primary Oscillator Frequency Range Configuration bits->Primary oscillator/external clock input frequency greater than 8MHz

#ifdef EXT_OSC
#pragma config POSCMOD = EC    // Primary Oscillator Configuration bits->External clock mode selected
#endif // EXT_OSC

#ifdef INT_OSC
#pragma config POSCMOD = NONE    // Primary Oscillator Configuration bits->High speed osc mode selected
#endif // INT_OSC

#pragma config SOSCSEL = SOSCHP    // SOSC Power Selection Configuration bits->Secondary Oscillator configured for high-power operation
#pragma config OSCIOFNC = OFF    // CLKO Enable Configuration bit->CLKO output disabled
#pragma config FCKSM = CSDCMD    // Clock Switching and Monitor Selection->Both Clock Switching and Fail-safe Clock Monitor are disabled

// FWDT
#pragma config WDTPS = PS16384    // Watchdog Timer Postscale Select bits->1:16384
#pragma config FWPSA = PR128    // WDT Prescaler bit->WDT prescaler ratio of 1:128
#pragma config WINDIS = OFF    // Windowed Watchdog Timer Disable bit->Standard WDT selected(windowed WDT disabled)
#pragma config FWDTEN = SWON    // WDT controlled with the SWDTEN bit setting

// FPOR
#pragma config I2C1SEL = PRI    // Alternate I2C1 Pin Mapping bit->Use Default SCL1/SDA1 Pins For I2C1
#pragma config BOREN = BOR3    // Brown-out Reset Enable bits->Brown-out Reset enabled in hardware, SBOREN bit disabled
#pragma config LVRCFG = OFF    // ->Low Voltage regulator is not available
#pragma config MCLRE = ON    // MCLR Pin Enable bit->RA5 input pin disabled,MCLR pin enabled
#pragma config BORV = V18    // Brown-out Reset Voltage bits->Brown-out Reset set to lowest voltage (1.8V)
#pragma config PWRTEN = ON    // Power-up Timer Enable bit->PWRT enabled

// FICD
#pragma config ICS = PGx3    // ICD Pin Placement Select bits->EMUC/EMUD share PGC1/PGD1

// FDS
#pragma config DSWDTPS = DSWDTPSF    // Deep Sleep Watchdog Timer Postscale Select bits->1:2,147,483,648 (25.7 Days)
#pragma config DSWDTOSC = LPRC    // DSWDT Reference Clock Select bit->DSWDT uses Low Power RC Oscillator (LPRC)
#pragma config DSBOREN = ON    // Deep Sleep Zero-Power BOR Enable bit->Deep Sleep BOR enabled in Deep Sleep
#pragma config DSWDTEN = OFF    // Deep Sleep Watchdog Timer Enable bit->DSWDT disabled

void proc_init(void) {
    /****************************************************************************
     * Setting the GPIO of PORTA
     ***************************************************************************/
    LATA = 0x00;
    TRISA = 0x0000;
    ODCA = 0x0010; // configure A4 as open drain
    /****************************************************************************
     * Setting the GPIO of PORTB
     ***************************************************************************/
    LATB = 0x00;
    TRISB = 0x0400;

    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR
     ***************************************************************************/
    ANSA = 0x0001; // disable analog inputs, except AN0
    ANSB = 0x0000; // disable analog inputs

//    PORTAbits.RA3 = 1;
    TRISBbits.TRISB0 = 0; // pin 4, GPS RX, output
    TRISBbits.TRISB1 = 1; // pin 5, GPS TX, input
    TRISBbits.TRISB2 = 1; // pin 6, RX, input 
    TRISBbits.TRISB3 = 0; // pin 7, LED1, output
    TRISBbits.TRISB4 = 1; // pin 11, GPIO w/ switch, input
    TRISBbits.TRISB7 = 0; // pin 16, TX, output
    TRISBbits.TRISB8 = 1; // pin 17, SCL, I2C periph
    TRISBbits.TRISB9 = 1; // pin 18, SDA, I2C periph
    TRISBbits.TRISB12 = 0; // pin 23, not used
    TRISBbits.TRISB14 = 1; // pin 25, PPS, input
    TRISAbits.TRISA0 = 1; // pin 2, VBAT
    TRISAbits.TRISA1 = 0; // pin 3, not used
    TRISAbits.TRISA2 = 1; // pin 9, not used
    TRISAbits.TRISA3 = 0; // pin 10, DO1, output
    TRISAbits.TRISA4 = 0; // pin 12, GPS RST, output ***CONFIG AS OPEN DRAIN***
    TRISAbits.TRISA7 = 0; // pin 19, LED2, output
    TRISBbits.TRISB15 = 1; // pin 26, T1, input
    TRISAbits.TRISA6 = 0; // pin 20, not used
    TRISBbits.TRISB13 = 0; // pin24, GPSINT

#ifdef INT_OSC
#ifdef CK32MHZ
    CLKDIV = 0x3000; // divide by one
#endif // CK32MHZ   
#ifdef CK16MHZ
    CLKDIV = 0x3100; // divide by 2
#endif // CK16MHZ   
#ifdef CK8MHZ
    CLKDIV = 0x3000; // divide by one
#endif // CK8MHZ   
#ifdef CK4MHZ
    CLKDIV = 0x3100; // divide by 2
#endif // CK4MHZ   
#endif
}

// declare message buffers
static char msg_buffer_mosi[MOSI_BFR_SIZE]; // master out, slave in
static char msg_buffer_miso[MISO_BFR_SIZE]; //master in, slave out
static char msg_buffer_ui[UI_BFR_SIZE]; // user interface input
static char msg_buffer_gp[GP_BFR_SIZE]; // genral purpose buffer
#ifdef BUILD_EEPROM
static char msg_buffer_log[LOG_BFR_SIZE]; // data log messages
#endif // BUILD_EEPROM
#ifdef BUILD_GPS
static char msg_buffer_gps[GPS_BFR_SIZE]; // messages from gps

const char cmd_e1[] = {'e', 0, '1'}; // command to initiate gps measurement
#endif // BUILD_GPS


void start_screen() {
    printf("\033[2J"); // clear screen
    printf("\033[f"); // home position
    printf("System started...\n\n\r");
    printf(">");
}

int config_init(void) {
    int err = 0;
    char* inptr;
    char* content;
    char* bufr;
    char keyword[] = "CONFIG"; // configuration keyword
    bufr = get_msg_buffer(GP); // use the general purpose buffer
    nvm_read_string(NVM_CONFIG_PAGE, bufr); // read the config string from nvm
    inptr = parse_frm(bufr, get_token_ptrs(), START_FLAG, END_FLAG, DELIMITER, 1);
    if (bufr == inptr) err = 1; // returned same pointer it was sent, means failed parse
    // if the string keyword matches the configuration keyword, load the config values
    if (!str_match(get_token_ptrs()[0], keyword, str_len(keyword), IGNORE_CASE)) {
        add_token_ptr(0, keyword); // build CONFIG frame
        add_token_ptr(CYCLE0, CYCLE0_DEFAULT);
        add_token_ptr(CYCLE1, CYCLE1_DEFAULT);
        add_token_ptr(CYCLE2, CYCLE2_DEFAULT);
        add_token_ptr(CYCLE3, CYCLE3_DEFAULT);
        add_token_ptr(TICKS, TICKS_DEFAULT);
        add_token_ptr(OSC_TUN, OSC_TUN_DEFAULT);
        content = build_frm(bufr, get_token_ptrs(), CONFIG_CNT + 1, START_FLAG, END_FLAG, DELIMITER);
        // write the new config string to non volatile memory
        nvm_write_string(NVM_CONFIG_PAGE, content);
    }
    return err;
}

int config_load(void) { // read config values from nvm and initialize cycle timers
    char* inptr;
    char* bufr;
    int err = 0;
    bufr = get_msg_buffer(GP);
    nvm_read_string(NVM_CONFIG_PAGE, bufr); // get the config frame and parse
    inptr = parse_frm(bufr, get_token_ptrs(), START_FLAG, END_FLAG, DELIMITER, 1);
    if (bufr == inptr) err = 1; // returned same pointer it was sent, means failed parse
    else { // put config values in the cycle timers
        cycle_timer_set(CYCLE0, atol(get_token_ptrs()[CYCLE0]), 0);
        cycle_timer_set(CYCLE1, atol(get_token_ptrs()[CYCLE1]), 0);
        cycle_timer_set(CYCLE2, atol(get_token_ptrs()[CYCLE2]), 0);
        cycle_timer_set(CYCLE3, atol(get_token_ptrs()[CYCLE3]), 0);
    }
    return err;
}

void set_msg_buffers(void) {
    // assign message buffers to mailboxes
    assign_msg_buffer(MOSI, msg_buffer_mosi); // master out, slave in
    assign_msg_buffer(MISO, msg_buffer_miso); //master in, slave out
    assign_msg_buffer(UI, msg_buffer_ui); // user interface input
    assign_msg_buffer(GP, msg_buffer_gp); // general purpose buffer
#ifdef BUILD_GPS
    assign_msg_buffer(GPS, msg_buffer_gps); // messages from gps
#endif // BUILD_GPS
#ifdef BUILD_EEPROM
    assign_msg_buffer(LOG, msg_buffer_log); // data log messages
#endif // BUILD_EEPROM
}

void initialize_i2c(void) {
    #ifdef BUILD_I2C1_MASTER
    initI2C1(BRG);
#endif // BUILD_I2C1_MASTER
#ifdef BUILD_I2C2_MASTER
    initI2C2(BRG);
#endif // BUILD_I2C2_MASTER
      
#ifdef BUILD_I2C1_SLAVE
    initI2C1(BRG, assign_msg_buffer(MOSI, msg_buffer_mosi), assign_msg_buffer(MISO, msg_buffer_miso));
#endif // BUILD_I2C1_SLAVE
}




#include "i2c_driver.h"
#define TIMEOUT 10000

int err = 0;

int i2c_err(void){
    return err;
}

void clr_i2c_err(void) {
    err = 0;
}

#ifdef BUILD_I2C1_MASTER

void initI2C1(int brg) {
    I2C1BRG = brg;
    I2C1CONbits.I2CEN = 1; // Enable I2C Mode
}

void startI2C1(void) {
    int i = TIMEOUT;
    I2C1CONbits.SEN = 1;
    while ((I2C1CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 1;
}

void restartI2C1(void) { // repeated start
    int i = TIMEOUT;
    I2C1CONbits.RSEN = 1; // initiate restart
    while ((I2C1CONbits.RSEN) && i--); // wait until restart finished
    if(!i) err = 1;
}

void stopI2C1(void) {
    int i = TIMEOUT;
    I2C1CONbits.PEN = 1;
    while ((I2C1CONbits.PEN) && i--); // wait until stop is finished
    if(!i) err = 2;
}

int putI2C1(int c) {
    I2C1TRN = c;
    int i = TIMEOUT;
    while ((I2C1STATbits.TRSTAT) && i--); // wait until 8 bits and return ack bit sent
    if(!i) err = 3;
    return(I2C1STATbits.ACKSTAT);
}

char getI2C1(int acksend) { // acksend is bit to send as ack after byte read
    int in_byte;
    int i = TIMEOUT;
    while ((I2C1CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C1CONbits.RCEN = 1; // receive enable
    i = TIMEOUT;
    while ((I2C1CONbits.RCEN) && i--); // *******************
    if(!i) err = 4;
    I2C1STATbits.I2COV = 0; // *******************
    i = TIMEOUT;
    while ((!I2C1STATbits.RBF) && i--); // wait for byte received
    if(!i) err = 4;
    in_byte = I2C1RCV; // get it
    i = TIMEOUT;
    while ((I2C1CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C1CONbits.ACKDT = acksend; // ack/nak bit to be sent
    I2C1CONbits.ACKEN = 1; // send the ack/nak bit
    i = TIMEOUT;
    while ((I2C1CONbits.ACKEN) && i--); // wait until complete
    if(!i) err = 4;
    return in_byte;
}

char getSI2C1(void) { // sends ack until '\0' received, then sends nak
    int in_byte;
    int i = TIMEOUT;
    while ((I2C1CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C1CONbits.RCEN = 1; // receive enable
    i = TIMEOUT;
    while ((I2C1CONbits.RCEN) && i--); // *******************
    if(!i) err = 4;
    I2C1STATbits.I2COV = 0; // *******************
    i = TIMEOUT;
    while ((!I2C1STATbits.RBF) && i--); // wait for byte received
    if(!i) err = 4;
    in_byte = I2C1RCV; // get it
    i = TIMEOUT;
    while ((I2C1CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    if(in_byte) I2C1CONbits.ACKDT = I2C_ACK; // ack/nak bit to be sent
    else I2C1CONbits.ACKDT = I2C_NAK;
    I2C1CONbits.ACKEN = 1; // send the ack/nak bit
    i = TIMEOUT;
    while ((I2C1CONbits.ACKEN) && i--); // wait until complete
    if(!i) err = 4;
    return in_byte;
}


void write1I2C1(char addr, char c) { // write 1 byte to slave
    startI2C1();
    putI2C1(addr & WADR);
    putI2C1(c);
    stopI2C1();
}

void write2I2C1(char addr, char c1, char c2) { // write 2 bytes to slave
    startI2C1();
    putI2C1(addr & WADR);
    putI2C1(c1);
    putI2C1(c2);
    stopI2C1();
}

void writeNI2C1(char addr, char* data, int cnt){ //write n bytes to slave
    int i;
#ifdef DEBUG    
    mst_trigger(0);
#endif
    startI2C1();
    putI2C1(addr & WADR);
    for(i=0; i<cnt; i++){
        putI2C1(*data);
        data++;
    }
    stopI2C1();
}

void read1I2C1(char addr, char* c) { // read 1 byte to c
    startI2C1();
    if (!putI2C1(addr | RADR)) {
        putI2C1(addr | RADR);
        *c = getI2C1(I2C_NAK); // end of read
    }
    stopI2C1();

}

void read2I2C1(char addr, char* c1, char* c2) { // read 2 bytes from slave
    startI2C1();
    if (!putI2C1(addr | RADR)) {
        *c1 = getI2C1(I2C_ACK);
        *c2 = getI2C1(I2C_NAK);
    }
    stopI2C1();

}

void readNI2C1(char addr, char* data, int cnt) { // read n bytes from slave
    int i;
    startI2C1();
    if (!putI2C1(addr | RADR)) {
        for (i = 0; i < cnt; i++) {
            if (i != cnt - 1) *data = getI2C1(I2C_ACK);
            else *data = getI2C1(I2C_NAK);
            data++;
        }

        stopI2C1();
    }
}

void readSI2C1(char addr, char* data) { // read a string from slave   
    char hold;
#ifdef DEBUG    
    mst_trigger(0);
#endif
    startI2C1();
    if (!putI2C1(addr | RADR)) {
        do {
            *data = getSI2C1();
            hold = *data;
            data++;
        } while (hold != 0);
    }
    stopI2C1();
}

void writeSI2C1(char addr, char* s) { //write string to slave
#ifdef DEBUG    
//    mst_trigger(0);
#endif
    startI2C1();
    putI2C1(addr & WADR);
    while (*s != 0) {
        putI2C1(*s);
        s++;
    } 
    putI2C1(*s); // send end of string
    stopI2C1();
}

#endif //BUILD_I2C1_MASTER

#ifdef BUILD_I2C2_MASTER

void initI2C2(int brg) {
    I2C2BRG = brg;
    I2C2CONbits.I2CEN = 1; // Enable I2C Mode
}

void startI2C2(void) {
    int i = TIMEOUT;
    I2C2CONbits.SEN = 1;
    while ((I2C2CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 1;
}

void restartI2C2(void) { // repeated start
    int i = TIMEOUT;
    I2C2CONbits.RSEN = 1; // initiate restart
    while ((I2C2CONbits.RSEN) && i--); // wait until restart finished
    if(!i) err = 1;
}

void stopI2C2(void) {
    int i = TIMEOUT;
    I2C2CONbits.PEN = 1;
    while ((I2C2CONbits.PEN) && i--); // wait until stop is finished
    if(!i) err = 2;
}

int putI2C2(int c) {
    I2C2TRN = c;
    int i = TIMEOUT;
    while ((I2C2STATbits.TRSTAT) && i--); // wait until 8 bits and return ack bit sent
    if(!i) err = 3;
    return(I2C2STATbits.ACKSTAT);
}

char getI2C2(int acksend) { // acksend is bit to send as ack after byte read
    int in_byte;
    int i = TIMEOUT;
    while ((I2C2CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C2CONbits.RCEN = 1; // receive enable
    i = TIMEOUT;
    while ((I2C2CONbits.RCEN) && i--); // *******************
    if(!i) err = 4;
    I2C2STATbits.I2COV = 0; // *******************
    i = TIMEOUT;
    while ((!I2C2STATbits.RBF) && i--); // wait for byte received
    if(!i) err = 4;
    in_byte = I2C2RCV; // get it
    i = TIMEOUT;
    while ((I2C2CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C2CONbits.ACKDT = acksend; // ack/nak bit to be sent
    I2C2CONbits.ACKEN = 1; // send the ack/nak bit
    i = TIMEOUT;
    while ((I2C2CONbits.ACKEN) && i--); // wait until complete
    if(!i) {
        err = 4;
    }
    return in_byte;
}

char getSI2C2(void) { // sends ack until '\0' received, then sends nak
    int in_byte;
    int i = TIMEOUT;
    while ((I2C2CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    I2C2CONbits.RCEN = 1; // receive enable
    i = TIMEOUT;
    while ((I2C2CONbits.RCEN) && i--); // *******************
    if(!i) err = 4;
    I2C2STATbits.I2COV = 0; // *******************
    i = TIMEOUT;
    while ((!I2C2STATbits.RBF) && i--); // wait for byte received
    if(!i) err = 4;
    in_byte = I2C2RCV; // get it
    i = TIMEOUT;
    while ((I2C2CON & 0x1f) && i--); // wait for bus idle
    if(!i) err = 4;
    if(in_byte) I2C2CONbits.ACKDT = I2C_ACK; // ack/nak bit to be sent
    else I2C2CONbits.ACKDT = I2C_NAK;
    I2C2CONbits.ACKEN = 1; // send the ack/nak bit
    i = TIMEOUT;
    while ((I2C2CONbits.ACKEN) && i--); // wait until complete
    if(!i) err = 4;
    return in_byte;
}


void write1I2C2(char addr, char c) { // write 1 byte to slave
    startI2C2();
    putI2C2(addr & WADR);
    putI2C2(c);
    stopI2C2();
}

void write2I2C2(char addr, char c1, char c2) { // write 2 bytes to slave
    startI2C2();
    putI2C2(addr & WADR);
    putI2C2(c1);
    putI2C2(c2);
    stopI2C2();
}

void writeNI2C2(char addr, char* data, int cnt){ //write n bytes to slave
    int i;
#ifdef DEBUG    
    mst_trigger(0);
#endif
    startI2C2();
    putI2C2(addr & WADR);
    for(i=0; i<cnt; i++){
        putI2C2(*data);
        data++;
    }
    stopI2C2();
}

void read1I2C2(char addr, char* c) { // read 1 byte to c
    startI2C2();
    if (!putI2C2(addr | RADR)) {
        *c = getI2C2(I2C_NAK); // end of read
    }
    stopI2C2();
}

void read2I2C2(char addr, char* c1, char* c2) { // read 2 bytes from slave
    startI2C2();
    if (!putI2C2(addr | RADR)) {
        *c1 = getI2C2(I2C_ACK);
        *c2 = getI2C2(I2C_NAK);
    }
    stopI2C2();
}

void readNI2C2(char addr, char* data, int cnt) { // read n bytes from slave
    int i;
    startI2C2();
    if (!putI2C2(addr | RADR)) {
        for (i = 0; i < cnt; i++) {
            if (i != cnt - 1) *data = getI2C2(I2C_ACK);
            else *data = getI2C2(I2C_NAK);
            data++;
        }
    }
    stopI2C2();
}

void readSI2C2(char addr, char* data) { // read a string from slave   
    char hold;
    startI2C2();
    if (!putI2C2(addr | RADR)) {
        do {
            *data = getSI2C2();
            hold = *data;
            data++;
        } while (hold != 0);
    }
    stopI2C2();
}

void writeSI2C2(char addr, char* s) { //write string to slave
#ifdef DEBUG    
    mst_trigger(0);
#endif
    startI2C2();
    putI2C2(addr & WADR);
    while (*s != 0) {
        putI2C2(*s);
        s++;
    } 
    putI2C2(*s); // send end of string
    stopI2C2();
}

#endif //BUILD_I2C2_MASTER


#ifdef DEBUG
void mst_trigger(int number) {
    int i;
    switch (number) {
        case 0:
            PORTAbits.RA3 = 1;
            Nop();
            for(i=0;i<10;i++) {
            Nop();
            }
            PORTAbits.RA3 = 0;
            break;
        case 1:
            PORTBbits.RB4 = 1;
            Nop();
            for(i=0;i<10;i++) {
            Nop();
            }
            PORTBbits.RB4 = 0;
            break;
    }
}
#endif // DEBUG

#ifdef BUILD_I2C1_SLAVE

typedef enum {GET_ADDR, GET_DATA, SEND_DATA, SEND_LAST} STATE;
#define READ 1
#define WRITE 0
volatile STATE status = GET_ADDR;
volatile unsigned int idx;
char* mosibuf;
char* misobuf;
char* hold;

//static int msg_avail = 0; // flag to indicate when message has been received

//void repoint_misobuf(*char newbuf){
//    hold = misobuf;
//    misobuf = newbuf;
//}
//
//void restore_misobuf(void){
//    misobuf = hold;
//}

void initI2C1(int brg, char* mosi_buffer, char* miso_buffer) {
    mosibuf = &mosi_buffer[0];
    misobuf = &miso_buffer[0];
    I2C1BRG = brg;
    I2C1ADD = (I2C_SLAVE_BASE_ADR + MODULE_ID - 1);
    IPC4bits.SI2C1P = 7;
    IFS1bits.MI2C1IF = 0; // Clear Interrupt Flag
    IEC1bits.SI2C1IE = 1; // enable interrupt
    I2C1CONbits.I2CEN = 1; // enable I2C
}

void __attribute__((interrupt,no_auto_psv)) _SI2C1Interrupt(void){
    char c;
    switch (status) {
        case GET_ADDR: // expecting device address
            idx = 0; // index to start of buffer
            c = I2C1RCV; // read device address to clear rx register
            if (I2C1STATbits.R_W == READ) { // master wants data
                I2C1TRN = misobuf[idx];  // give data
                if (misobuf[idx] == 0) status = SEND_LAST;
                else status = SEND_DATA; // update state to SEND_DATA
                idx++;
                I2C1CONbits.SCLREL = 1; // release SCL
                }
            else status = GET_DATA; // master wants to send data
            break;
        case GET_DATA: // expecting data
            mosibuf[idx++] = I2C1RCV; // get data
            if (mosibuf[idx-1] == 0) {  // if null, end of string, update status
                status = GET_ADDR;
                set_msg_flag(MOSI); // indicate that message is received and ready to parse
            }
            break;
        case SEND_DATA:
            if (misobuf[idx] == 0) status = SEND_LAST;
            I2C1TRN = misobuf[idx];
            idx++;
            I2C1CONbits.SCLREL = 1;
            break;
        case SEND_LAST:
            status = GET_ADDR;
            break;
        default:
            status = GET_ADDR;
    } //switch
    IFS1bits.SI2C1IF = 0;
} //isr

#endif //BUILD_I2C1_SLAVE

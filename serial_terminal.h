/* 
 * File:   serial_terminal.h
 * Author: WHalphen
 *
 * Created on December 17, 2015, 11:33 AM
 * 
 * PURPOSE
 * Provides functions for initiation of UART peripheral, character output, and
 * interrupt driven receive for a user terminal.  
 * 
 * Received characters are put in a buffer.  When
 * a carriage return is received, a message ready flag is set to indicate that
 * a line of input is ready for processing.  No more input is accepted until
 * another process reads the buffer content and clears the message ready flag.
 * 
 * Input is echoed to the UART transmitter.  Backspace removes the previous 
 * character from the buffer and sends directions to the terminal to clear the
 * previous echoed character.
 * 
 * When the buffer is full, no more input is accepted, except for backspace or
 * carriage return.  A bell instruction is sent to the terminal to indicate full
 * buffer.
 * 
 * FUNCTIONS
 * extern void ser_init(int baudratereg2); -- Initializes the uart and sets
 * the baud rate.  This should be called once before using the uart
 *   PARAMETER: baudratereg2 is the defined baud rate register value
 * 
 * void __attribute__((interrupt(auto_psv))) _U2RXInterrupt(void); -- this 
 * interrupt service routine is executed each time a character is received by
 * the UART peripheral
 * 
 * extern void  ser_putchar(char c); -- Outputs a single character to the 
 * uart transmitter
 *   PARAMETER: c is the character to be output
 * 
 * void set_ui_msg_rdy(void); -- This method is used to set the message
 * ready flag
 * 
 * void clr_ui_msg_rdy(void); -- This method is used to clear the message ready
 * flag.  This should be done by external processes immediately upon reading
 * the contents of the buffer.  While this flag is set, no input is accepted.
 * 
 * int chk_ui_msg_rdy(void); -- This method returns the value of the message
 * ready flag and should be used by external processes that monitor the input
 * from the user terminal.
 * 
 * char* ui_bfr(void); -- This method returns the address of the buffer.  It
 * should be used by external processes to retrieve the location of the input
 * string to be processed.
 * 
 */

#ifndef SERIAL_TERMINAL_H
#define	SERIAL_TERMINAL_H

#include "sys_config.h"

#define BS 8 // ascii backspace
#define CR 13 // ascii carriage return
#define BEL 7 // ascii bell
#define DEL 0x7f // ascii delete
#define SP 0x20 // ascii space
#define LF 0x0a // line feed

#ifdef UI_UART1
void __attribute__((interrupt(auto_psv))) _U1RXInterrupt(void); // ISR
#endif // UI_UART1
#ifdef UI_UART2
void __attribute__((interrupt(auto_psv))) _U2RXInterrupt(void); // ISR
#endif // UI_UART2
extern void ser_init(int baudratereg, int* msg_rdy_flg, char* msg_buffer ); // initialize uart2
extern void  ser_putchar(char c); //output a character, c, to the terminal
void set_ui_msg_rdy(void); // set the message ready flag
void clr_ui_msg_rdy(void); // clear the message ready flag
int chk_ui_msg_rdy(void); // read the message ready flag
char* ui_bfr(void); // returns the address of the start of the buffer

#endif	/* SERIAL_TERMINAL_H */

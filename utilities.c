
#include "utilities.h"
#include <xc.h>

static int trace = 0;
static int debug = 0;

int str_len(char* s) { // returns length of string, s, without null
    int ctr = 0;
    while(*s != 0) {
        ctr++;
        s++;
    }
    return ctr;
}

char i_to_ascii_nib(char nibble){ // converts a 4 bit integer to ascii 0-F
    if ((nibble & 0x0f) < 0x0a) return (nibble & 0x0f) + 48;
    else return (nibble & 0x0f) + 55;    
    }

char ascii_nib_to_i(char nibble) { // converts an ASCII hex nibble to integer 0-F
    if ((nibble > 47) & (nibble < 58)) return (nibble - 48);
    if ((nibble > 64) & (nibble < 71)) return (nibble - 55);
    return 0xf0; // if not valid hex character
}
char ucase(char c) { // converts c to upper case
    if (c >= 'a' && c <= 'z') c -= 32;
    return c;
}

int str_match(char *str1, char *str2, int len, int ignore_case) {
    int i;
    for (i = 0; i < len; i++) {
        if (ignore_case) {
            if (ucase(str1[i]) != ucase(str2[i])) return 0;
        } else if (str1[i] != str2[i]) return 0;
    }
    return 1;
}

char* copy_string(char* dest, char* src) {
    while(*src) {
        *dest = *src;
        src++;
        dest++;
    } 
    *dest = '\0';
    return dest;
}

void wdt(int on_off) { // 1: on, 0: off
    RCONbits.SWDTEN = on_off;
}

void wdt_clr(void) {
    ClrWdt();
}

// Error handler functions

int last_err[MAX_MODULES];

// clear error code for specified module, returns cleared error code
int err_clear(int module) { 
    int err;
    err = last_err[module];
    last_err[module] = 0;
    return err;
}

// sets error code for specified module if not zero, returns error code
int err_set(int module, int error_code) { 
    last_err[module] = error_code;
    return error_code;
}

// initializes last_err array values to 0
void err_init(void) { 
    int i;
    for (i = 0; i < MAX_MODULES; i++) last_err[i] = 0;
}

void set_trace(int state) { // 1: trace on, 0: trace off
    trace = state;
}

int get_trace(void) {
    return trace;
}

void set_debug(int state) { // 1: debug on, 0: debug off
    debug = state;
}

int get_debug(void) {
    return debug;
}

/* 
 * File:   pressure.h
 * Author: WHalphen
 *
 * Created on March 21, 2016, 12:34 PM
 */
/*
 * FUNCTION
 *  void pressure_reset(void);
 *   function to send reset sequence to sensor
 * 
 * FUNCTION
 * unsigned long pressure_adc(char cmd);
 *  Perform sensor analog to digital conversion
 *  Parameter cmd indicates conversion time/ precision
 * 
 * FUNCTION
 * unsigned int pressure_prom(char coef_num);
 *  Read calibration coefficient from sensor
 *  Parameter 'coef_num' indicates which coefficient is to be read
 * 
 * FUNCTION
 * void pressure_crc4(void);
 *  calculate the CRC code
 * 
 * FUNCTION
 * void pressure_init(void);
 *  initialize the sensor
 * 
 * FUNCTION
 * unsigned int get_press_calib(int coeff);
 *  return the indicated calibration coeff
 * 
 * FUNCTION
 * unsigned char get_press_crc(void);
 *  return the CRC value
 *
 */
#include "sys_config.h"
#ifdef BUILD_PRESSURE
#include "i2c_driver.h"
#include "timebase.h"
#include <math.h>

#ifndef PRESSURE_H
#define	PRESSURE_H

#define ADDR_W 0xEE // Module address write mode
#define ADDR_R 0xEF // Module address read mode
#define CMD_RESET 0x1E // ADC reset command
#define CMD_ADC_READ 0x00 // ADC read command
#define CMD_ADC_CONV 0x40 // ADC conversion command
#define CMD_ADC_D1 0x00 // ADC D1 conversion
#define CMD_ADC_D2 0x10 // ADC D2 conversion
#define CMD_ADC_256 0x00 // ADC OSR=256
#define CMD_ADC_512 0x02 // ADC OSR=512
#define CMD_ADC_1024 0x04 // ADC OSR=1024
#define CMD_ADC_2048 0x06 // ADC OSR=2048
#define CMD_ADC_4096 0x08 // ADC OSR=4096
#define CMD_PROM_RD 0xA0 // Prom read command 

void pressure_reset(void); // send reset sequence
unsigned long pressure_adc(char cmd); //  perform adc conversion
unsigned int pressure_prom(char coef_num); //  read calibration coefficients
void pressure_crc4(void); //  calculate the CRC code
void pressure_init(void); // initialize sensor
unsigned int get_press_calib(int coeff);
unsigned char get_press_crc(void);
void pressure_values(long* T, long* P);

#endif	/* PRESSURE_H */
#endif // BUILD_PRESSURE
